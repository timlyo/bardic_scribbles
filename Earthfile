VERSION 0.6

rust:
    FROM rust:1.66
    WORKDIR /bardic_scribbles

rust-tools:
    FROM +rust

    RUN cargo install cargo-quickinstall
    RUN cargo quickinstall cargo-chef

    RUN echo "Installed:" && ls $CARGO_HOME/bin
    SAVE ARTIFACT $CARGO_HOME/bin/*

prepare-cache:
    FROM +rust-tools

    COPY --dir src Cargo.lock Cargo.toml .
    RUN cargo chef prepare
    SAVE ARTIFACT recipe.json

build-cache:
    FROM +rust

    COPY +rust-tools/cargo-chef $CARGO_HOME/bin/

    COPY +prepare-cache/recipe.json ./
    RUN cargo chef cook --release

    RUN du -sh target
    RUN du -sh $CARGO_HOME

    SAVE ARTIFACT target
    SAVE ARTIFACT $CARGO_HOME cargo_home

    SAVE IMAGE --push registry.gitlab.com/timlyo/bardic_scribbles/build_cache:latest

build:
    FROM +build-cache

    COPY --dir src Cargo.lock Cargo.toml .
    COPY --dir emails .
    COPY --dir templates/compiled templates/

    RUN cargo build --release

    RUN ldd target/release/bardic_scribbles

    SAVE ARTIFACT target/release/bardic_scribbles

dist:
    FROM node:lts

    COPY package.json package-lock.json webpack.config.js .

    RUN npm install

    COPY --dir src/assets src/assets

    RUN npm run build

    SAVE ARTIFACT dist

build-image:
    FROM debian:stable-slim

    BUILD +build-cache

    WORKDIR bardic_scribbles

    ARG DEBIAN_FRONTEND=noninteractive

    RUN apt-get update && apt-get install -y libssl1.1 ca-certificates && rm -rf /var/lib/apt/lists/*

    COPY +build/bardic_scribbles .
    COPY --dir templates .

    COPY --dir +dist/dist .

    EXPOSE 8080
    CMD ["./bardic_scribbles"]

    ARG tags="latest"

    FOR tag IN $tags
        SAVE IMAGE --push registry.gitlab.com/timlyo/bardic_scribbles/bardic_scribbles:$tag
    END

push-production-image:
    FROM +build-image
    SAVE IMAGE --push registry.fly.io/bs-app:latest