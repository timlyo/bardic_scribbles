#!/usr/bin/env just --justfile

build_bundle:
    npm run build

migrate_production:
    sudo wg-quick up bardicscribbles

    edgedb migrate -I fly

    sudo wg-quick down bardicscribbles

shell_in_fly:
    sudo wg-quick up bardicscribbles

    fish

    sudo wg-quick down bardicscribbles

start_jaeger:
    docker run -d -p6831:6831/udp -p6832:6832/udp -p16686:16686 -p14268:14268 jaegertracing/all-in-one:latest

open_jaeger:
    firefox localhost:16686