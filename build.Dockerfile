FROM rust:latest

RUN DEBIAN_FRONTEND=noninteractive apt update\
  && apt install -y npm firefox-esr\
  # Cypress deps
  && apt install -y libgtk2.0-0 libgtk-3-0 libgbm-dev libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 libxtst6 xauth xvfb\
  && rm -rf /var/lib/apt/lists/*

RUN cargo install sqlx-cli --locked --no-default-features --features postgres --features rustls\
    && rm -rf ~/.cargo/registry/ && rm -rf ~/.cargo/git

ENV PATH="~/.cargo/bin:${PATH}"

RUN rustup component add clippy