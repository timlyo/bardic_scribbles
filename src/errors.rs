use crate::database::users::EdgeDbError;
use axum::body;
use axum::extract::rejection::ExtensionRejection;
use axum::http::StatusCode;
use axum::response::{IntoResponse, Response};
use std::fmt::{Display, Formatter};
use std::ops::Deref;
use tracing_error::SpanTrace;

#[derive(Debug)]
pub struct Error {
    error_type: ErrorType,
    context: SpanTrace,
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}\n{}", self.error_type, self.context)
    }
}

impl Error {
    pub fn bad_request(message: impl ToString) -> Self {
        Self {
            error_type: ErrorType::BadRequest(message.to_string()),
            context: SpanTrace::capture(),
        }
    }

    pub fn not_found(id: impl ToString) -> Self {
        Self {
            error_type: ErrorType::NotFound { id: id.to_string() },
            context: SpanTrace::capture(),
        }
    }

    pub fn unauthorised(message: impl ToString) -> Self {
        Self {
            error_type: ErrorType::Unauthorised(message.to_string()),
            context: SpanTrace::capture(),
        }
    }
}

impl<T> From<T> for Error
where
    T: Into<ErrorType>,
{
    fn from(error_type: T) -> Self {
        Self {
            error_type: error_type.into(),
            context: SpanTrace::capture(),
        }
    }
}

impl Deref for Error {
    type Target = ErrorType;

    fn deref(&self) -> &Self::Target {
        &self.error_type
    }
}

#[derive(thiserror::Error, Debug)]
pub enum ErrorType {
    #[error("404 Couldn't find {id}")]
    NotFound { id: String },
    #[error("Authorisation Error: {0}")]
    Unauthorised(String),
    #[error("Database Error {0:?}")]
    EdgeDb(#[from] EdgeDbError),
    #[error("Bad Request: {0}")]
    BadRequest(String),
    #[error("Internal Error: {0}")]
    Internal(String),
    #[error("Template Error: {0:?}")]
    Template(#[from] tera::Error),
    #[error("Template Error: {0:?}")]
    CompiledTemplate(#[from] askama::Error),
    #[error("Error processing extension {0:?}")]
    ExtensionRejection(#[from] ExtensionRejection),
}

impl IntoResponse for ErrorType {
    fn into_response(self) -> Response {
        let status = self.as_status();

        if self.should_log() {
            tracing::error!(message=%self.to_string(), %status);
        } else {
            tracing::warn!(message=%self.to_string(), %status);
        }

        let msg = body::boxed(body::Full::from(format!("{:#?}", self)));

        Response::builder().status(status).body(msg).unwrap()
    }
}

impl IntoResponse for Error {
    fn into_response(self) -> Response {
        let status = self.as_status();
        let context = self.context.to_string();

        if self.should_log() {
            tracing::error!(message=%self.to_string(), %status, %context);
        } else {
            tracing::warn!(message=%self.to_string(), %status, %context);
        }

        let msg = body::boxed(body::Full::from(format!(
            "{:#?}\n{}",
            self.error_type, context
        )));

        Response::builder().status(status).body(msg).unwrap()
    }
}

impl ErrorType {
    pub fn should_log(&self) -> bool {
        matches!(
            self,
            ErrorType::NotFound { .. }
                | ErrorType::Internal(_)
                | ErrorType::Template(_)
                | ErrorType::ExtensionRejection(_)
        )
    }

    pub fn as_status(&self) -> StatusCode {
        match self {
            ErrorType::NotFound { .. } => StatusCode::NOT_FOUND,
            ErrorType::Unauthorised(_) => StatusCode::UNAUTHORIZED,
            ErrorType::BadRequest(_) => StatusCode::BAD_REQUEST,
            ErrorType::EdgeDb(_)
            | ErrorType::Internal(_)
            | ErrorType::Template(_)
            | ErrorType::CompiledTemplate(_)
            | ErrorType::ExtensionRejection(_) => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }

    pub fn not_found(id: impl ToString) -> ErrorType {
        ErrorType::NotFound { id: id.to_string() }
    }

    pub fn bad_request(msg: impl ToString) -> ErrorType {
        ErrorType::BadRequest(msg.to_string())
    }

    pub fn internal(msg: impl ToString) -> ErrorType {
        ErrorType::Internal(msg.to_string())
    }
}
