use crate::errors::{Error, ErrorType};
use reqwest::Response;
use tera::Tera;
use tokio::sync::OnceCell;

const URL: &str = "https://api.eu.mailgun.net/v3/mg.bardicscribbles.com/messages";

const WELCOME: &str = include_str!("../emails/welcome.mjml");

fn parse_mjml(content: &str) -> Result<String, ErrorType> {
    let root = mrml::parse(content).map_err(ErrorType::internal)?;
    let options = mrml::prelude::render::Options::default();

    root.render(&options).map_err(ErrorType::internal)
}

async fn get_templates() -> Result<&'static Tera, ErrorType> {
    static INSTANCE: OnceCell<Tera> = OnceCell::const_new();

    INSTANCE
        .get_or_try_init(|| async {
            let welcome = parse_mjml(WELCOME)?;

            let mut tera = Tera::default();

            tera.add_raw_template("welcome.html", &welcome)?;

            Ok(tera)
        })
        .await
}

#[derive(serde::Serialize)]
struct EmailBody<'a> {
    pub to: &'a str,
    pub from: &'static str,
    pub subject: &'a str,
    pub html: &'a str,
    #[serde(rename = "o:tag")]
    pub tag: &'a str,
}

#[derive(serde::Serialize, Debug)]
pub struct WelcomeParams {
    pub username: String,
    pub verification_link: String,
}

#[tracing::instrument]
pub async fn send_welcome_email(email: &str, params: WelcomeParams) -> Result<Response, Error> {
    let templates = get_templates().await?;

    let context = tera::Context::from_serialize(&params)?;
    let html = templates.render("welcome.html", &context)?;

    send_email(email, "Welcome to Bardic Scribbles", &html, "welcome").await
}

#[tracing::instrument(skip(html))]
pub async fn send_email(
    email: &str,
    subject: &str,
    html: &str,
    tag: &str,
) -> Result<Response, Error> {
    let api_key = std::env::var("MAILGUN_API_KEY").map_err(ErrorType::internal)?;

    let client = reqwest::Client::new();

    let body = EmailBody {
        to: email,
        from: "no-reply@bardicscribbles.com",
        subject,
        html,
        tag,
    };

    let response = client
        .post(URL)
        .basic_auth("api", Some(api_key))
        .form(&body)
        .send()
        .await;

    match response {
        Ok(res) => res
            .error_for_status()
            .map_err(|e| ErrorType::internal(e).into()),
        Err(e) => Err(ErrorType::internal(e).into()),
    }
}
