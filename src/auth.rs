use crate::errors::Error;
use crate::models::collection::Collection;
use crate::models::scribble::Scribble;
use crate::models::user::{User, UserType};
use crate::models::votes::VoteType;
use std::collections::HashSet;
use uuid::Uuid;

#[derive(Debug, Eq, PartialEq, Hash)]
pub enum Permission {
    View,
    Edit,
    Delete,
    Vote(VoteType),
}

#[derive(Debug)]
pub enum Resource {
    Scribble { owner: Uuid },
    User { user_id: Uuid },
    Collection { owner: Uuid, public: bool },
    AdminResource,
    ModeratorResource,
}

impl From<&Scribble> for Resource {
    fn from(val: &Scribble) -> Self {
        Resource::Scribble {
            owner: val.creator.id,
        }
    }
}

impl From<&Collection> for Resource {
    fn from(val: &Collection) -> Self {
        Resource::Collection {
            owner: val.owner_id,
            public: val.public,
        }
    }
}

impl From<&User> for Resource {
    fn from(val: &User) -> Self {
        Resource::User { user_id: val.id }
    }
}

#[derive(Debug)]
pub struct Authorisation<'a> {
    pub permissions: HashSet<Permission>,
    /// User doing operation
    pub auditor: Option<&'a User>,
    pub resource: Resource,
}

impl<'a> Authorisation<'a> {
    pub fn edit_or_error(&self) -> Result<(), Error> {
        if self.permissions.contains(&Permission::Edit) {
            Ok(())
        } else {
            Err(Error::unauthorised(
                "You aren't authorised to edit this resource",
            ))
        }
    }

    pub fn delete_or_error(&self) -> Result<(), Error> {
        if self.permissions.contains(&Permission::Delete) {
            Ok(())
        } else {
            Err(Error::unauthorised(
                "You aren't authorised to delete this resource",
            ))
        }
    }

    pub fn view_or_error(&self) -> Result<(), Error> {
        if self.permissions.contains(&Permission::View) {
            Ok(())
        } else {
            Err(Error::unauthorised(
                "You aren't authorised to view this resource",
            ))
        }
    }

    pub fn vote_or_error(&self, vote_type: VoteType) -> Result<(), Error> {
        if let (Resource::Scribble { owner }, Some(auditor)) = (&self.resource, self.auditor) {
            if *owner == auditor.id {
                return Err(Error::unauthorised("You can't vote on your own scribble"));
            }
        }

        if self.permissions.contains(&Permission::Vote(vote_type)) {
            Ok(())
        } else {
            Err(Error::unauthorised(format!(
                "You aren't authorised for this vote: {vote_type}"
            )))
        }
    }
}

mod permission_sets {
    use crate::auth::Permission;
    use std::collections::HashSet;

    pub fn only_view() -> HashSet<Permission> {
        HashSet::from([Permission::View])
    }

    pub fn none() -> HashSet<Permission> {
        HashSet::new()
    }

    pub fn permissions_for_own_user() -> HashSet<Permission> {
        HashSet::from([Permission::View, Permission::Edit])
    }

    pub fn permissions_for_own_scribble() -> HashSet<Permission> {
        HashSet::from([Permission::View, Permission::Edit, Permission::Delete])
    }

    pub fn permissions_for_own_collection() -> HashSet<Permission> {
        HashSet::from([Permission::View, Permission::Edit, Permission::Delete])
    }
}

pub fn get_authorisation<'a>(
    auditor: impl Into<Option<&'a User>>,
    resource: impl Into<Resource>,
) -> Authorisation<'a> {
    get_authorisation_inner(auditor.into(), resource.into())
}

#[tracing::instrument]
fn get_authorisation_inner(auditor: Option<&User>, resource: Resource) -> Authorisation {
    tracing::info!(message = "Checking Authorisation");

    let permissions = if let Some(auditor) = auditor {
        get_permissions_for_user(auditor, &resource)
    } else {
        get_permissions_for_anon(&resource)
    };

    Authorisation {
        permissions,
        auditor,
        resource,
    }
}

fn get_permissions_for_anon(resource: &Resource) -> HashSet<Permission> {
    use permission_sets::*;

    match &resource {
        Resource::Scribble { .. } => only_view(),

        Resource::User { .. } => only_view(),

        Resource::Collection { public: true, .. } => only_view(),
        Resource::Collection { public: false, .. } => none(),
        Resource::AdminResource | Resource::ModeratorResource => none(),
    }
}

fn get_permissions_for_user(auditor: &User, resource: &Resource) -> HashSet<Permission> {
    use permission_sets::*;

    match resource {
        Resource::Scribble { owner } => {
            if auditor.id == *owner {
                permissions_for_own_scribble()
            } else {
                let mut permissions = only_view();
                permissions.extend(get_vote_set(auditor.points));
                permissions
            }
        }

        Resource::User { user_id: owner } => {
            if auditor.id == *owner {
                permissions_for_own_user()
            } else {
                only_view()
            }
        }

        Resource::Collection { owner, public } => {
            if auditor.id == *owner {
                permissions_for_own_collection()
            } else if *public {
                only_view()
            } else {
                none()
            }
        }
        Resource::AdminResource => {
            if auditor.get_type() == UserType::Admin {
                only_view()
            } else {
                none()
            }
        }
        Resource::ModeratorResource => match auditor.get_type() {
            UserType::Pleb => none(),
            UserType::Moderator | UserType::Admin => only_view(),
        },
    }
}

/// Get the list of vote types that a user is able to express
fn get_vote_set(user_points: i64) -> HashSet<Permission> {
    let vote_types = match user_points {
        _ => [VoteType::Basic],
    };

    HashSet::from_iter(
        vote_types
            .into_iter()
            .map(|vote_type| Permission::Vote(vote_type)),
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use edgedb_protocol::model::Datetime;

    fn test_user() -> User {
        User {
            id: Uuid::new_v4(),
            username: "test".to_string(),
            created: Datetime::UNIX_EPOCH,
            email: None,
            user_type_str: "Pleb".to_string(),
            points: 0,
            verified_email: false,
            verification_code: None,
        }
    }

    #[test]
    fn user_can_edit_own_scribble() {
        let user = test_user();
        let resource = Resource::Scribble { owner: user.id };

        let auth = get_authorisation(Some(&user), resource);

        assert!(auth.permissions.contains(&Permission::Edit));
    }

    #[test]
    fn user_cannot_edit_other_users_scribble() {
        let user = test_user();
        let resource = Resource::Scribble {
            owner: Uuid::new_v4(),
        };

        let auth = get_authorisation(Some(&user), resource);

        assert!(!auth.permissions.contains(&Permission::Edit));
    }
}
