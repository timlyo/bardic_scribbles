use axum_flash::{IncomingFlashes, Level};
use tower_cookies::{Cookie, PrivateCookies};
use uuid::Uuid;

pub fn set_login_cookie(cookies: &PrivateCookies, user_id: Uuid) {
    cookies.add(Cookie::new("user_id", user_id.to_string()));
}

#[derive(serde::Serialize, serde::Deserialize, Debug)]
pub struct Flash<'a> {
    pub class: &'static str,
    pub title: Option<&'a str>,
    pub message: &'a str,
}

pub fn get_flashes(flashes: &IncomingFlashes) -> Vec<Flash> {
    flashes
        .iter()
        .map(|(level, message)| {
            let class = match level {
                Level::Debug => "",
                Level::Info => "info",
                Level::Success => "success",
                Level::Warning => "warning",
                Level::Error => "danger",
            };

            if let Some((title, message)) = message.split_once(':') {
                Flash {
                    class,
                    title: Some(title),
                    message,
                }
            } else {
                Flash {
                    class,
                    title: None,
                    message,
                }
            }
        })
        .collect()
}
