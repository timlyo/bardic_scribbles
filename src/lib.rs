extern crate core;

use crate::models::user::User;
use crate::templates::Template;
use axum::extract::FromRef;
use axum::response::{IntoResponse, Redirect, Response};
use axum::Extension;
use axum_flash::Flash;
use once_cell::sync::OnceCell;
use opentelemetry::sdk::trace::Tracer;
use sentry::ClientInitGuard;
use std::env;
use std::fmt::{Display, Formatter};
use std::net::SocketAddr;
use tera::Tera;
use tower::ServiceBuilder;
use tower_cookies::CookieManagerLayer;
use tower_http::compression::CompressionLayer;
use tower_http::trace::{DefaultMakeSpan, DefaultOnResponse, TraceLayer};
use tracing::Level;
use tracing_opentelemetry::OpenTelemetryLayer;
use tracing_subscriber::layer::SubscriberExt;
use tracing_subscriber::util::SubscriberInitExt;
use tracing_subscriber::{EnvFilter, Layer, Registry};

mod auth;
mod database;
mod emails;
mod errors;
mod extractors;
mod forms;
mod models;
mod passwords;
mod routes;
mod templates;
mod util;

static TERA: OnceCell<Tera> = OnceCell::new();

#[derive(Debug, Clone, Copy)]
pub enum Environment {
    Development,
    Production,
}

impl Display for Environment {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Environment::Development => write!(f, "Development"),
            Environment::Production => write!(f, "Production"),
        }
    }
}

impl Environment {
    pub fn current() -> Self {
        if env::var("FLY_APP_NAME").is_ok() {
            Environment::Production
        } else {
            Environment::Development
        }
    }
}

#[derive(Clone)]
pub struct Resources {
    edge_db: edgedb_tokio::Client,
    cookie_key: tower_cookies::Key,
    flash_config: axum_flash::Config,
}

impl FromRef<Resources> for axum_flash::Config {
    fn from_ref(state: &Resources) -> axum_flash::Config {
        state.flash_config.clone()
    }
}

pub enum Responses {
    Template(Template),
    Redirect(Redirect),
    FlashRedirect((Flash, Redirect)),
}

impl From<Template> for Responses {
    fn from(template: Template) -> Self {
        Responses::Template(template)
    }
}

impl From<Redirect> for Responses {
    fn from(redirect: Redirect) -> Self {
        Responses::Redirect(redirect)
    }
}

impl IntoResponse for Responses {
    #[tracing::instrument(skip(self))]
    fn into_response(self) -> Response {
        match self {
            Responses::Template(template) => template.into_response(),
            Responses::Redirect(redirect) => redirect.into_response(),
            Responses::FlashRedirect((_flash, _redirect)) => unimplemented!(),
        }
    }
}

#[tracing::instrument]
pub async fn main() -> Result<(), hyper::Error> {
    if let Err(e) = dotenv::dotenv() {
        println!("Warning: failed to setup dotenv: {}", e);
    }

    let environment = Environment::current();

    let _guard = initialise_sentry(environment);

    tracing_subscriber::registry()
        .with(create_telemetry_layer())
        .with(
            tracing_subscriber::fmt::layer()
                .compact()
                .with_filter(EnvFilter::from_default_env()),
        )
        .with(tracing_error::ErrorLayer::default())
        .with(sentry_tracing::layer())
        .init();

    TERA.set({
        let mut tera = Tera::new("templates/**/*.html.tera").unwrap();
        tera.autoescape_on(vec![".html", ".html.tera"]);
        tera
    })
    .unwrap();

    let edge_db = edgedb_tokio::create_client().await.unwrap();
    tracing::info!(message = "Connected to database");

    let cookie_key = match env::var("SECRET_KEY").ok() {
        Some(variable_key) => {
            tracing::info!(message = "Loading Cookie key from env variable");
            tower_cookies::Key::from(variable_key.as_bytes())
        }
        None => tower_cookies::Key::generate(),
    };

    let resources = Resources {
        edge_db,
        cookie_key: cookie_key.clone(),
        flash_config: axum_flash::Config::new(cookie_key.clone()),
    };

    let request_logger = TraceLayer::new_for_http()
        .make_span_with(DefaultMakeSpan::new().level(Level::INFO))
        .on_response(DefaultOnResponse::new().level(Level::INFO));

    let service = ServiceBuilder::new()
        .layer(request_logger)
        .layer(CompressionLayer::new().deflate(true).gzip(true).br(true))
        .layer(CookieManagerLayer::new());

    let app = routes::router()
        .with_state(resources.clone())
        .layer(service)
        .layer(Extension(resources));

    let address = get_address();

    tracing::info!(message="Starting server", %address, %environment);

    axum::Server::bind(&address)
        .serve(app.into_make_service())
        .await
}

fn initialise_sentry(environment: Environment) -> ClientInitGuard {
    sentry::init((
        "https://0d12a176b8f140ab9bf6bd137d789b79@o289818.ingest.sentry.io/6253272",
        sentry::ClientOptions {
            release: sentry::release_name!(),
            environment: Some(environment.to_string().into()),
            ..Default::default()
        },
    ))
}

fn create_telemetry_layer() -> Option<OpenTelemetryLayer<Registry, Tracer>> {
    if env::var("BS_TRACING").is_ok() {
        let tracer = opentelemetry_jaeger::new_agent_pipeline()
            .with_service_name("bs-web")
            .install_batch(opentelemetry::runtime::Tokio)
            .unwrap();
        Some(tracing_opentelemetry::layer().with_tracer(tracer))
    } else {
        None
    }
}

fn get_address() -> SocketAddr {
    if let Ok(port) = env::var("BS_PORT") {
        format!("0.0.0.0:{}", port)
            .parse()
            .expect("Failed to parse socket addr")
    } else {
        "0.0.0.0:5000".parse().unwrap()
    }
}
