import css from "../css/style.scss";

import * as timeago from "timeago.js";
import Tagify from "@yaireo/tagify";

document.addEventListener("DOMContentLoaded", function main() {
  import("instant.page");
  const times = document.querySelectorAll(".timeago");
  if (times.length > 0) {
    timeago.render(times, "en_GB");
  }

  const tag_input = document.querySelector("#tag-input");
  if (tag_input) {
    let tagify = new Tagify(tag_input, {
      originalInputValueFormat: (valuesArr) =>
        valuesArr.map((item) => item.value).join(","),
      dropdown: {
        enabled: 0,
        maxItems: 5,
        position: "text",
        highlightFirst: true,
      },
    });

    fetch("/tags_json")
      .then((response) => response.json())
      .then((data) => (tagify.whitelist = data))
      .then(() => console.log("Loaded tags", tagify.whitelist));
  }

  const editor = document.querySelector("#editor");
  if (editor) {
    initialise_editor(editor);
  }

  // Get all "navbar-burger" elements
  const navbar_burgers = Array.prototype.slice.call(
    document.querySelectorAll(".navbar-burger"),
    0
  );

  // Check if there are any navbar burgers
  if (navbar_burgers.length > 0) {
    // Add a click event on each of them
    navbar_burgers.forEach((el) => {
      el.addEventListener("click", () => {
        // Get the target from the "data-target" attribute
        const target = el.dataset.target;
        const $target = document.getElementById(target);

        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        el.classList.toggle("is-active");
        $target.classList.toggle("is-active");
      });
    });
  }
});

async function initialise_editor(editor) {
  const scribble_body = document.querySelector("#scribble-body");

  const { default: Quill } = await import(
    /* webpackPreload: true */
    "quill/dist/quill.js"
  );

  // Set initial content to scribble body in form
  editor.innerHTML = scribble_body.value;

  console.log("Initialising editor");
  let quill = new Quill(editor, {
    theme: "snow",
  });

  // Hide existing editor so that only the quill editor shows
  scribble_body.style.display = "none";

  // Send contents of editor on submit
  document
    .querySelector("#editor")
    .parentElement.parentElement.addEventListener("submit", function (event) {
      // Replaces normal form body with the HTML contained in the quill editor
      scribble_body.value = quill.root.innerHTML;
    });
}
