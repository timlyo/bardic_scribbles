use edgedb_derive::Queryable;
use uuid::Uuid;

#[derive(
    strum::IntoStaticStr,
    Clone,
    Copy,
    serde::Deserialize,
    Debug,
    Hash,
    Eq,
    PartialEq,
    strum::EnumIter,
    strum::Display,
)]
pub enum VoteType {
    #[serde(alias = "basic")]
    Basic,
    #[serde(alias = "detail")]
    Detail,
    #[serde(alias = "unique")]
    Unique,
    #[serde(alias = "modular")]
    Modular,
    #[serde(alias = "super_vote")]
    SuperVote,
    #[serde(alias = "unfocused")]
    Unfocused,
    #[serde(alias = "needs_more_detail")]
    NeedsMoreDetail,
    #[serde(alias = "problematic")]
    Problematic,
}

impl VoteType {
    pub fn value_and_cost(&self) -> (i32, i32) {
        match self {
            VoteType::Basic => (1, 0),
            VoteType::Detail => (5, 1),
            VoteType::Unique => (5, 1),
            VoteType::Modular => (5, 1),
            VoteType::SuperVote => (10, 10),
            VoteType::Unfocused => (-5, 1),
            VoteType::NeedsMoreDetail => (-5, 1),
            VoteType::Problematic => (-5, 1),
        }
    }
}

#[derive(Queryable)]
pub struct VoteCount {
    pub id: Uuid,
    pub votes: i64,
    pub vote_type: String,
}

#[derive(serde::Serialize, Clone)]
pub struct VoteCounts {
    pub basic: i64,
    pub detail: i64,
    pub unique: i64,
    pub modular: i64,
    pub super_vote: i64,
    pub unfocused: i64,
    pub needs_more_detail: i64,
    pub problematic: i64,
}

impl VoteCounts {
    pub fn value_for(&self, vote_type: VoteType) -> i64 {
        match vote_type {
            VoteType::Basic => self.basic,
            VoteType::Detail => self.detail,
            VoteType::Unique => self.unique,
            VoteType::Modular => self.modular,
            VoteType::SuperVote => self.super_vote,
            VoteType::Unfocused => self.unfocused,
            VoteType::NeedsMoreDetail => self.needs_more_detail,
            VoteType::Problematic => self.problematic,
        }
    }
}

#[derive(serde::Serialize, Clone)]
pub struct UserVotes {
    pub basic: Option<Vote>,
    pub detail: Option<Vote>,
    pub unique: Option<Vote>,
    pub modular: Option<Vote>,
    pub super_vote: Option<Vote>,
    pub unfocused: Option<Vote>,
    pub needs_more_detail: Option<Vote>,
    pub problematic: Option<Vote>,
}

impl UserVotes {
    pub fn contains_vote(&self, vote_type: VoteType) -> bool {
        match vote_type {
            VoteType::Basic => self.basic.is_some(),
            VoteType::Detail => self.detail.is_some(),
            VoteType::Unique => self.unique.is_some(),
            VoteType::Modular => self.modular.is_some(),
            VoteType::SuperVote => self.super_vote.is_some(),
            VoteType::Unfocused => self.unfocused.is_some(),
            VoteType::NeedsMoreDetail => self.needs_more_detail.is_some(),
            VoteType::Problematic => self.problematic.is_some(),
        }
    }
}

#[derive(serde::Serialize, Queryable, Clone)]
pub struct Vote {
    pub id: Uuid,
    pub vote_type_str: String,
    pub voter_name: String,
    pub voter_id: Uuid,
    pub reason: Option<String>,
}

#[derive(Debug, Queryable)]
pub struct VoteId {
    #[allow(dead_code)]
    id: Uuid,
}
