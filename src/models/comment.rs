use edgedb_derive::Queryable;
use edgedb_protocol::model::Datetime;
use serde::Serialize;
use serde_with::{serde_as, DisplayFromStr};
use std::collections::HashMap;
use uuid::Uuid;

#[serde_as]
#[derive(Serialize, Debug, Queryable)]
pub struct Comment {
    id: Uuid,
    creator: CommentCreator,
    content: String,
    #[serde_as(as = "DisplayFromStr")]
    created: Datetime,
    positive_votes: i64,
    negative_votes: i64,
    /// `Some(true)` if upvoted, `Some(false)` if down, `None` if no vote
    user_positive_vote: Option<bool>,
    parent: Option<CommentId>,
    children: Vec<CommentId>,
}

pub struct CommentSoup {
    pub comments: HashMap<Uuid, Comment>,
    pub roots: Vec<Uuid>,
}

impl CommentSoup {
    pub fn from_comments(raw_comments: Vec<Comment>) -> Self {
        let mut comments = HashMap::with_capacity(raw_comments.len());
        let mut roots = Vec::new();

        for comment in raw_comments {
            if comment.parent.is_none() {
                roots.push(comment.id)
            }

            comments.insert(comment.id, comment);
        }

        Self { comments, roots }
    }

    fn get_branch(comments: &mut HashMap<Uuid, Comment>, root_id: Uuid) -> Option<CommentLeaf> {
        if let Some(comment) = comments.remove(&root_id) {
            let children = comment
                .children
                .iter()
                .filter_map(|child| CommentSoup::get_branch(comments, child.id))
                .collect();

            Some(CommentLeaf { comment, children })
        } else {
            None
        }
    }

    pub fn construct_tree(mut self) -> Vec<CommentLeaf> {
        self.roots
            .iter()
            .filter_map(|root| CommentSoup::get_branch(&mut self.comments, *root))
            .collect()
    }
}

#[derive(Serialize)]
pub struct CommentLeaf {
    #[serde(flatten)]
    comment: Comment,
    children: Vec<CommentLeaf>,
}

#[derive(Serialize, Debug, Queryable)]
pub struct CommentVote {
    id: Uuid,
    pub positive: bool,
}

#[derive(Serialize, Debug, Queryable)]
pub struct CommentCreator {
    id: Uuid,
    username: String,
}

#[derive(Serialize, Debug, Queryable)]
pub struct CommentId {
    pub id: Uuid,
}
