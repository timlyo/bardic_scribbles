use edgedb_derive::Queryable;
use edgedb_protocol::model::Datetime;
use serde_with::{serde_as, DisplayFromStr};
use uuid::Uuid;

#[derive(serde::Serialize, Debug, Queryable)]
pub struct CollectionId {
    pub id: Uuid,
}

#[serde_as]
#[derive(serde::Serialize, Debug, Queryable)]
pub struct Collection {
    pub id: Uuid,
    pub name: String,
    #[serde_as(as = "DisplayFromStr")]
    pub created: Datetime,
    pub owner_id: Uuid,
    pub public: bool,
}

#[serde_as]
#[derive(serde::Serialize, Debug, Queryable)]
pub struct CollectionForScribbleAndUser {
    pub id: Uuid,
    pub name: String,
    #[serde_as(as = "DisplayFromStr")]
    pub created: Datetime,
    pub owner_id: Uuid,
    pub public: bool,
    pub scribble_in_collection: bool,
}
