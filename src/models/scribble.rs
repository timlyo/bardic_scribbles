use edgedb_derive::Queryable;
use edgedb_protocol::model::Datetime;
use serde_with::{serde_as, DisplayFromStr};
use uuid::Uuid;

#[serde_as]
#[derive(serde::Serialize, Clone, Debug, Queryable)]
pub struct ScribbleSummary {
    pub id: Uuid,
    pub title: String,
    #[serde_as(as = "DisplayFromStr")]
    pub created: Datetime,
    pub creator: User,
    pub tags: Vec<Tag>,
    pub points: i64,
}

#[derive(serde::Serialize, Clone, Debug, Queryable)]
pub struct Tag {
    pub id: Uuid,
    pub name: String,
}

#[derive(serde::Serialize, Clone, Debug, Queryable)]
pub struct User {
    pub id: Uuid,
    pub username: String,
    pub user_type_str: String,
}

#[derive(serde::Serialize, Debug, Queryable)]
pub struct ScribbleId {
    pub id: Uuid,
}

#[serde_as]
#[derive(serde::Serialize, Debug, Queryable)]
pub struct Scribble {
    pub id: Uuid,
    #[serde_as(as = "DisplayFromStr")]
    pub created: Datetime,
    pub creator: User,
    pub latest_body: ScribbleBody,
    pub tags: Vec<ScribbleTag>,
    pub points: i64,
}

#[serde_as]
#[derive(serde::Serialize, Debug, Queryable)]
pub struct ScribbleBody {
    pub id: Uuid,
    #[serde_as(as = "DisplayFromStr")]
    pub created: Datetime,
    pub title: String,
    pub body: String,
    pub edit_reason: Option<String>,
}

#[derive(serde::Serialize, Debug, Queryable)]
pub struct ScribbleTag {
    pub id: Uuid,
    pub name: String,
    pub description: Option<String>,
}

#[derive(serde::Serialize, Debug, Queryable)]
pub struct ScribbleTagId {
    pub id: Uuid,
}
