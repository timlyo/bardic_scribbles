use crate::models::scribble::ScribbleSummary;
use edgedb_derive::Queryable;
use edgedb_protocol::model::Datetime;
use serde::{Deserialize, Serialize};
use serde_with::{serde_as, DisplayFromStr};
use uuid::Uuid;

#[serde_as]
#[derive(serde::Serialize, Debug, Clone, Queryable)]
pub struct User {
    pub id: Uuid,
    pub username: String,
    #[serde_as(as = "DisplayFromStr")]
    pub created: Datetime,
    pub email: Option<String>,
    pub user_type_str: String,
    pub points: i64,
    pub verified_email: bool,
    pub verification_code: Option<Uuid>,
}

impl User {
    pub fn get_type(&self) -> UserType {
        match self.user_type_str.as_str() {
            "Pleb" => UserType::Pleb,
            "Moderator" => UserType::Moderator,
            "Admin" => UserType::Admin,
            other => panic!("Invalid user_type: {}", other),
        }
    }
}

#[derive(Queryable)]
pub struct UserId {
    pub id: Uuid,
}

#[serde_as]
#[derive(serde::Serialize, Debug, Clone, Queryable)]
pub struct UserDetailed {
    pub id: Uuid,
    pub username: String,
    #[serde_as(as = "DisplayFromStr")]
    pub created: Datetime,
    pub email: Option<String>,
    pub user_type_str: String,
    pub points: i64,
    pub verified_email: bool,
    pub verification_code: Option<Uuid>,
    pub scribbles: Vec<ScribbleSummary>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Copy, Clone)]
pub enum UserType {
    Pleb,
    Moderator,
    Admin,
}

#[derive(serde::Serialize, Debug, Queryable)]
pub struct UserStats {
    pub scribble_count: i64,
    pub comment_count: i64,
}

#[derive(Queryable)]
pub struct UserPasswordHash {
    pub id: Uuid,
    pub password_hash: String,
}
