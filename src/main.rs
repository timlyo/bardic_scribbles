#[tokio::main]
pub async fn main() -> Result<(), hyper::Error> {
    bardic_scribbles::main().await
}
