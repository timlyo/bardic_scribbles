use crate::models::votes::VoteType;

#[derive(serde::Deserialize, Debug)]
pub struct Vote {
    pub vote: VoteType,
    pub reason: Option<String>,
}
