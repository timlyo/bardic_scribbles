#![allow(clippy::needless_borrow)]

use crate::forms::rendering::button_input::ButtonInput;
use crate::forms::rendering::text_area_input::TextAreaInput;
use crate::forms::rendering::text_input::TextInput;
use crate::forms::rendering::{FormErrors, FormInput, RenderForm};
use serde::Serialize;
use std::collections::{HashMap, HashSet};
use validator::Validate;

#[derive(serde::Deserialize, Debug, Serialize, validator::Validate)]
pub struct ScribbleData {
    #[validate(length(min = 3, message = "Too Short"))]
    pub title: String,
    #[validate(length(min = 10, message = "Must be longer than 10 characters"))]
    pub body: String,
    pub tags: String,
    pub edit_reason: Option<String>,
}

impl RenderForm for ScribbleData {
    fn get_inputs() -> Vec<FormInput> {
        vec![
            FormInput::new("title", TextInput::text()).required(),
            FormInput::new("scribble-body", TextAreaInput::default())
                .name("body")
                .title("Body")
                .postfix("<div id='editor' class='block'></div>")
                .required(),
            FormInput::new("tag-input", TextInput::text())
                .title("Tags")
                .name("tags"),
            FormInput::new("submit-button", ButtonInput::submit()).title("Submit"),
        ]
    }

    fn get_values(&self) -> HashMap<&'static str, String> {
        let mut values = HashMap::from([
            ("title", self.title.clone()),
            ("body", self.body.clone()),
            ("tags", self.tags.clone()),
        ]);

        if let Some(edit_reason) = self.edit_reason.clone() {
            values.insert("edit_reason", edit_reason);
        }

        values
    }

    fn get_errors(&self) -> FormErrors {
        match self.validate() {
            Err(e) => e.into(),
            Ok(()) => FormErrors::default(),
        }
    }

    fn get_route_and_method() -> (&'static str, &'static str) {
        ("/scribbles", "POST")
    }
}

impl ScribbleData {
    pub fn get_tags(&self) -> HashSet<&str> {
        self.tags
            .split(',')
            .filter(|t| !t.is_empty())
            .into_iter()
            .map(str::trim)
            .collect()
    }
}
