use crate::forms::rendering::InputType;

pub struct TextAreaInput {
    pub content: String,
    pub rows: u32,
}

impl Default for TextAreaInput {
    fn default() -> Self {
        Self {
            content: String::new(),
            rows: 6,
        }
    }
}

impl From<TextAreaInput> for InputType {
    fn from(input: TextAreaInput) -> Self {
        InputType::TextArea(input)
    }
}
