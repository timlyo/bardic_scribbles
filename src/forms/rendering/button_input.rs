use crate::forms::rendering::InputType;

pub struct ButtonInput {
    pub r#type: String,
}

impl From<ButtonInput> for InputType {
    fn from(input: ButtonInput) -> Self {
        InputType::Button(input)
    }
}

impl ButtonInput {
    pub fn submit() -> Self {
        Self {
            r#type: String::from("submit"),
        }
    }
}
