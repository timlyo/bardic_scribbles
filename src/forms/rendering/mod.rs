use crate::forms::rendering::button_input::ButtonInput;
use crate::forms::rendering::text_area_input::TextAreaInput;
use crate::forms::rendering::text_input::TextInput;
use heck::ToTitleCase;
use itertools::Itertools;
use std::collections::HashMap;
use std::fmt::Display;
use validator::{ValidationErrors, ValidationErrorsKind};

pub mod button_input;
pub mod text_area_input;
pub mod text_input;

pub enum InputType {
    Text(TextInput),
    Button(ButtonInput),
    CheckBox,
    TextArea(TextAreaInput),
}

pub struct FormInput {
    id: String,
    title: String,
    name: String,
    help_text: Option<String>,
    error_text: Option<String>,
    input_type: InputType,
    value: Option<String>,
    postfix: Option<String>,
    required: bool,
    extra_classes: Vec<String>,
}

impl FormInput {
    pub fn new(id: impl ToString, input_type: impl Into<InputType>) -> Self {
        let id = id.to_string();
        let title = id.to_title_case();
        let name = id.clone();

        Self {
            id,
            title,
            name,
            help_text: None,
            error_text: None,
            input_type: input_type.into(),
            value: None,
            postfix: None,
            required: false,
            extra_classes: Vec::new(),
        }
    }

    pub fn help(self, text: impl Display) -> Self {
        Self {
            help_text: Some(text.to_string()),
            ..self
        }
    }

    pub fn title(self, text: impl Display) -> Self {
        Self {
            title: text.to_string(),
            ..self
        }
    }

    pub fn name(self, text: impl Display) -> Self {
        Self {
            name: text.to_string(),
            ..self
        }
    }

    pub fn postfix(self, content: impl Display) -> Self {
        Self {
            postfix: Some(content.to_string()),
            ..self
        }
    }

    pub fn required(self) -> Self {
        Self {
            required: true,
            ..self
        }
    }

    pub fn render(&self) -> String {
        let id = self.id.as_str();
        let title = self.title.as_str();
        let name = self.name.as_str();

        let help = self
            .help_text
            .as_deref()
            .map(|text| format!(r#"<p class="help">{text}</p>"#))
            .unwrap_or_default();

        let error = self
            .error_text
            .as_deref()
            .map(|text| format!(r#"<p class="help is-danger">{text}</p>"#))
            .unwrap_or_default();

        let value = self
            .value
            .as_deref()
            .map(|value| format!(r#"value="{value}""#))
            .unwrap_or_default();

        let required = if self.required {
            r#"required=true"#
        } else {
            ""
        };

        let label = format!(r#"<label for="{id}" class="label">{title}</label>"#);

        let extra_classes = self.extra_classes.join(" ");

        let postfix = self.postfix.as_deref().unwrap_or_default();

        let body = match &self.input_type {
            InputType::Text(text) => format!(
                r#"
                {label}
                {help}
                <div class="control {extra_classes}">
                    <input
                        type = "{input_type}"
                        id = "{id}"
                        name = "{name}"
                        class = "input"
                        {value}
                        {required}
                    >
                {error}
                {postfix}
                </div>
                "#,
                input_type = text.r#type,
            ),
            InputType::Button(button) => {
                format!(
                    r#"<button id="{id}" class="button {extra_classes}" type="{button_type}" {value}>{title}</button>"#,
                    button_type = button.r#type
                )
            }
            InputType::CheckBox => {
                format!(
                    r#"{label}
                <input type="checkbox" class="checkbox {extra_classes}" name="{name}">"#
                )
            }
            InputType::TextArea(text_area) => {
                format!(
                    r#"{label}
                    <textarea class="textarea {extra_classes}" id="{id}" name="{name}" rows="{rows}">
                        {content}
                    </textarea>
                    {postfix}
                    "#,
                    rows = text_area.rows,
                    content = text_area.content
                )
            }
        };

        format!(r#"<div class="field">{body}</div>"#)
    }
}

#[derive(Default)]
pub struct FormErrors {
    /// Errors that apply to the entire form
    pub general: Vec<String>,
    /// Errors that a apply to a specific field
    pub field: HashMap<String, String>,
}

impl FormErrors {
    pub fn has_errors(&self) -> bool {
        let is_empty = self.general.is_empty() && self.field.is_empty();

        !is_empty
    }
    pub fn get_field(&self, field_name: &str) -> Option<String> {
        self.field.get(field_name).cloned()
    }
}

impl From<ValidationErrors> for FormErrors {
    fn from(errors: ValidationErrors) -> Self {
        let mut result = FormErrors::default();

        for (name, error) in errors.errors() {
            match error {
                ValidationErrorsKind::Struct(_general) => {
                    todo!("get sub errors")
                }
                ValidationErrorsKind::List(_list) => {
                    todo!("Work out what the hell this is")
                }
                ValidationErrorsKind::Field(field) => {
                    let message = field.iter().map(|err| err.to_string()).collect();
                    result.field.insert(name.to_string(), message);
                }
            }
        }

        result
    }
}

pub trait RenderForm {
    fn get_inputs() -> Vec<FormInput>;

    fn get_values(&self) -> HashMap<&'static str, String>;

    fn get_errors(&self) -> FormErrors;

    fn get_route_and_method() -> (&'static str, &'static str);

    fn get_errors_optional(&self) -> Option<FormErrors> {
        let errors = self.get_errors();

        if errors.has_errors() {
            Some(errors)
        } else {
            None
        }
    }

    /// Render an empty version of the form
    #[tracing::instrument]
    fn render_empty() -> String {
        let inputs = Self::get_inputs()
            .into_iter()
            .map(|i| i.render())
            .join("\n");

        let (route, method) = Self::get_route_and_method();

        format!(r#"<form action="{route}" method="{method}">{inputs}</form>"#)
    }

    #[tracing::instrument(skip(self))]
    fn render(&self) -> String {
        self.render_with_errors(self.get_errors())
    }

    /// Render a version of the form with inline errors
    #[tracing::instrument(skip(self, errors))]
    fn render_with_errors(&self, errors: FormErrors) -> String {
        let values = self.get_values();

        let (route, method) = Self::get_route_and_method();

        let inputs = Self::get_inputs()
            .into_iter()
            .map(|input| {
                let value = input
                    .value
                    .or_else(|| values.get(input.id.as_str()).map(|v| v.to_string()));

                let error_text = input.error_text.or_else(|| errors.get_field(&input.id));

                FormInput {
                    value,
                    error_text,
                    ..input
                }
            })
            .map(|i| i.render())
            .join("\n");

        format!(r#"<form action="{route}" method="{method}">{inputs}</form>"#)
    }
}
