use crate::forms::rendering::InputType;

pub struct TextInput {
    pub r#type: String,
}

impl From<TextInput> for InputType {
    fn from(input: TextInput) -> Self {
        InputType::Text(input)
    }
}

impl TextInput {
    pub fn text() -> Self {
        Self {
            r#type: String::from("text"),
        }
    }

    pub fn password() -> Self {
        Self {
            r#type: String::from("password"),
        }
    }

    pub fn email() -> Self {
        Self {
            r#type: String::from("email"),
        }
    }
}
