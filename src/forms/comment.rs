use uuid::Uuid;

#[derive(serde::Deserialize, Debug)]
pub struct Comment {
    pub content: String,
    pub parent: Option<Uuid>,
}
