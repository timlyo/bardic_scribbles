use crate::forms::rendering::button_input::ButtonInput;
use crate::forms::rendering::text_input::TextInput;
use crate::forms::rendering::{FormErrors, FormInput, InputType, RenderForm};
use serde::de::Error;
use serde::{Deserialize, Deserializer};
use std::collections::HashMap;

#[derive(serde::Deserialize, Debug)]
pub struct Collection {
    pub name: String,
    #[serde(deserialize_with = "html_checkbox", default)]
    pub public: bool,
}

impl RenderForm for Collection {
    fn get_inputs() -> Vec<FormInput> {
        vec![
            FormInput::new("name", TextInput::text()),
            FormInput::new("public", InputType::CheckBox).title("Public Collection"),
            FormInput::new("submit", ButtonInput::submit()),
        ]
    }

    fn get_values(&self) -> HashMap<&'static str, String> {
        HashMap::from([("name", self.name.clone())])
    }

    fn get_errors(&self) -> FormErrors {
        FormErrors::default()
    }

    fn get_route_and_method() -> (&'static str, &'static str) {
        ("/collections", "POST")
    }
}

fn html_checkbox<'de, D>(d: D) -> Result<bool, D::Error>
where
    D: Deserializer<'de>,
{
    String::deserialize(d).and_then(|v| match v.as_str() {
        "on" => Ok(true),
        "off" => Ok(false),
        _ => Err(D::Error::custom("Invalid boolean value for form")),
    })
}
