use crate::forms::rendering::button_input::ButtonInput;
use crate::forms::rendering::text_input::TextInput;
use crate::forms::rendering::{FormErrors, FormInput, RenderForm};
use serde_with::serde_as;
use std::collections::HashMap;
use validator::Validate;

#[serde_as]
#[derive(serde::Deserialize, Debug, validator::Validate)]
pub struct NewUser {
    #[validate(length(min = 3, message = "Must be longer than 3 characters"))]
    pub username: String,
    #[validate(length(min = 8, message = "Must be longer than 8 characters"))]
    pub password: String,
    #[validate(email(message = "Must be a valid email"))]
    #[serde_as(as = "serde_with::NoneAsEmptyString")]
    pub email: Option<String>,
}

impl RenderForm for NewUser {
    fn get_inputs() -> Vec<FormInput> {
        vec![
            FormInput::new("username", TextInput::text()).required(),
            FormInput::new("password", TextInput::password()).required(),
            FormInput::new("email", TextInput::email())
                .help("Email is optional, but recommended. It is required for password resets. It will never be shared with anyone."),
            FormInput::new("submit-button", ButtonInput::submit()).title("Create User"),
        ]
    }

    fn get_values(&self) -> HashMap<&'static str, String> {
        let mut values = HashMap::from([
            ("username", self.username.clone()),
            ("password", self.password.clone()),
        ]);

        if let Some(email) = self.email.clone() {
            values.insert("email", email);
        }

        values
    }

    fn get_errors(&self) -> FormErrors {
        match self.validate() {
            Err(e) => e.into(),
            Ok(()) => FormErrors::default(),
        }
    }

    fn get_route_and_method() -> (&'static str, &'static str) {
        ("/users", "POST")
    }
}
