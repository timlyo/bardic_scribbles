use crate::errors::ErrorType;
use argon2::password_hash::rand_core::OsRng;
use argon2::password_hash::{Encoding, SaltString};
use argon2::{Argon2, PasswordHash, PasswordHasher, PasswordVerifier};

pub fn hash_password(password: &str) -> Result<String, ErrorType> {
    let salt = SaltString::generate(&mut OsRng);

    let argon2 = Argon2::default();

    argon2
        .hash_password(password.as_bytes(), &salt)
        .map(|p| p.to_string())
        .map_err(ErrorType::internal)
}

pub fn verify_password(
    password: &str,
    password_hash: &str,
) -> Result<(), argon2::password_hash::Error> {
    let argon2 = Argon2::default();
    let hash = PasswordHash::parse(password_hash, Encoding::default())?;

    argon2.verify_password(password.as_bytes(), &hash)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn passwords_can_be_hashed_and_verified() {
        let hash = hash_password("password").unwrap();
        assert!(verify_password("password", &hash).is_ok());
    }
}
