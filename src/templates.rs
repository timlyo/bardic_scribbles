use crate::auth::Authorisation;
use crate::auth::Permission::Vote;
use crate::models::user::User;
use crate::models::votes::{UserVotes, VoteCounts, VoteType};
use axum::body::boxed;
use axum::response::{Html, IntoResponse, Response};
use hyper::Body;
use serde_json::json;
use strum::IntoEnumIterator;
use uuid::Uuid;

pub struct Template {
    pub name: &'static str,
    pub current_user: Option<User>,
    pub context: serde_json::Value,
}

impl Template {
    pub fn no_context(name: &'static str, current_user: Option<User>) -> Self {
        Self {
            name,
            current_user,
            context: json!({}),
        }
    }

    pub fn with_context(
        name: &'static str,
        current_user: Option<User>,
        context: serde_json::Value,
    ) -> Self {
        Self {
            name,
            current_user,
            context,
        }
    }
}

impl IntoResponse for Template {
    #[tracing::instrument(name="render_template", skip(self), fields(name=self.name))]
    fn into_response(self) -> Response {
        let mut context = tera::Context::from_value(self.context)
            .expect("Failed to create tera context from value");

        if let Some(current_user) = self.current_user {
            context.insert("current_user", &current_user);
        }

        let tera = crate::TERA.get().expect("Tera instance wasn't created");

        let name = format!("{}.html.tera", self.name);

        match tera.render(&name, &context) {
            Ok(body) => Html(body).into_response(),
            Err(error) => {
                tracing::error!(message = "Error rendering template", ?error);
                Response::new(boxed(Body::from(error.to_string())))
            }
        }
    }
}

#[derive(askama::Template)]
#[template(path = "compiled/vote_buttons.html")]
pub struct VoteButtons<'a> {
    pub authorisation: Authorisation<'a>,
    pub votes: VoteCounts,
    /// List of votes which the current user has already cast on this scribble
    pub existing_user_vote: Option<UserVotes>,
    /// If current user owns scribble
    pub is_owner: bool,
    pub scribble_id: Uuid,
}

impl<'a> VoteButtons<'a> {
    fn get_vote_info(&self) -> Vec<VoteButtonInfo> {
        VoteType::iter()
            .filter_map(|vote_type| {
                self.show(vote_type).then(|| {
                    VoteButtonInfo::make(
                        vote_type,
                        self.votes.value_for(vote_type),
                        self.has_previous(vote_type),
                    )
                })
            })
            .collect()
    }

    fn is_authorised(&self, vote_type: VoteType) -> bool {
        self.authorisation.permissions.contains(&Vote(vote_type))
    }

    fn has_previous(&self, vote_type: VoteType) -> bool {
        self.existing_user_vote
            .as_ref()
            .map(|user_votes| user_votes.contains_vote(vote_type))
            .unwrap_or(false)
    }

    pub fn show(&self, vote_type: VoteType) -> bool {
        self.is_authorised(vote_type) || self.has_previous(vote_type)
    }
}

struct VoteButtonInfo {
    pub title: &'static str,
    pub value: &'static str,
    pub icon: &'static str,
    pub colour: &'static str,
    pub count: i64,
    /// True if current user has used this vote
    pub user_votes: bool,
}

impl VoteButtonInfo {
    pub fn make(vote_type: VoteType, count: i64, user_votes: bool) -> Self {
        match vote_type {
            VoteType::Basic => Self {
                title: "Basic Vote",
                value: "basic",
                icon: "fa-arrow-up",
                colour: "dark",
                count,
                user_votes,
            },
            VoteType::Detail => Self {
                title: "Detail",
                value: "detail",
                icon: "fa-search",
                colour: "success",
                count,
                user_votes,
            },
            VoteType::Unique => Self {
                title: "Unique",
                value: "unique",
                icon: "fa-snowflake",
                colour: "success",
                count,
                user_votes,
            },
            VoteType::Modular => Self {
                title: "Modular",
                value: "modular",
                icon: "fa-puzzle-piece",
                colour: "success",
                count,
                user_votes,
            },
            VoteType::SuperVote => Self {
                title: "Super Vote",
                value: "super_vote",
                icon: "fa-star",
                colour: "success",
                count,
                user_votes,
            },
            VoteType::Unfocused => Self {
                title: "Unfocused",
                value: "unfocused",
                icon: "fa-cloud",
                colour: "danger",
                count,
                user_votes,
            },
            VoteType::NeedsMoreDetail => Self {
                title: "Needs more detail",
                value: "needs_more_detail",
                icon: "fa-plus",
                colour: "danger",
                count,
                user_votes,
            },
            VoteType::Problematic => Self {
                title: "Problematic",
                value: "problematic",
                icon: "fa-exclamation-triangle",
                colour: "danger",
                count,
                user_votes,
            },
        }
    }
}
