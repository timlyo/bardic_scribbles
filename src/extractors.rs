use axum::extract::{Extension, FromRequestParts};
use axum::http::request::Parts;
use axum::RequestPartsExt;
use moka::future::Cache;
use std::time::Duration;
use tokio::sync::OnceCell;
use tower_cookies::Cookies;
use uuid::Uuid;

use crate::errors::Error;
use crate::models::user::User;
use crate::{database, Resources};

pub static USER_CACHE: OnceCell<moka::future::Cache<Uuid, User>> = OnceCell::const_new();

#[async_trait::async_trait]
impl<S> FromRequestParts<S> for User
where
    S: Send + Sync,
{
    type Rejection = Error;

    async fn from_request_parts(parts: &mut Parts, _state: &S) -> Result<Self, Self::Rejection> {
        let Extension(resources): Extension<Resources> = parts.extract().await?;
        let Extension(cookies): Extension<Cookies> = parts.extract().await?;

        let uuid = match cookies.private(&resources.cookie_key).get("user_id") {
            Some(cookie) => Uuid::parse_str(cookie.value()).map_err(Error::bad_request)?,
            None => return Err(Error::unauthorised("Not logged in")),
        };

        let cache = USER_CACHE
            .get_or_init(|| async {
                Cache::builder()
                    .time_to_live(Duration::from_secs(30))
                    .build()
            })
            .await;

        Ok(if let Some(user) = cache.get(&uuid) {
            tracing::debug!(message="Loaded user from cache", %uuid);
            user
        } else {
            let user = database::users::load_user(&resources.edge_db, uuid)
                .await
                .map_err(Error::from)?;

            tracing::debug!(message="Loaded user from db", %uuid);

            cache.insert(uuid, user.clone()).await;

            user
        })
    }
}
