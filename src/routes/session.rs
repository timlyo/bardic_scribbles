use axum::extract::{Extension, Form};
use axum::response::{IntoResponse, Redirect};
use axum::routing::{get, post};
use axum::Router;
use serde_json::json;
use tower_cookies::{Cookie, Cookies};

use crate::errors::ErrorType;
use crate::forms::session::Login;
use crate::models::user::User;
use crate::passwords::verify_password;
use crate::util::Flash;
use crate::{database, Resources, Responses, Template};

pub fn router() -> Router<Resources> {
    Router::new()
        .route("/login", get(get_login_page).post(login))
        .route("/logout", post(logout))
}

async fn get_login_page(current_user: Option<User>) -> Template {
    Template::no_context("login", current_user)
}

async fn login(
    Extension(resources): Extension<Resources>,
    cookies: Cookies,
    current_user: Option<User>,
    Form(login): Form<Login>,
) -> Result<Responses, ErrorType> {
    let auth = database::users::get_password_hash(&resources.edge_db, &login.username)
        .await?
        .and_then(|password_hash| {
            match verify_password(&login.password, &password_hash.password_hash) {
                Ok(()) => Some(password_hash.id),
                Err(error) => {
                    tracing::info!(message = "Password hash failed to validate", ?error);
                    None
                }
            }
        });

    if let Some(user_id) = auth {
        crate::util::set_login_cookie(&cookies.private(&resources.cookie_key), user_id);

        Ok(Responses::Redirect(Redirect::to(&format!(
            "/users/{user_id}"
        ))))
    } else {
        let flashes = vec![Flash {
            class: "warning",
            title: None,
            message: "Incorrect username or password",
        }];

        let context = json!({
            "error": "Incorrect username or password",
            "flashes": flashes
        });

        Ok(Responses::Template(Template::with_context(
            "login",
            current_user,
            context,
        )))
    }
}

async fn logout(cookies: Cookies, Extension(resources): Extension<Resources>) -> impl IntoResponse {
    cookies
        .private(&resources.cookie_key)
        .remove(Cookie::named("user_id"));

    Redirect::to("/")
}
