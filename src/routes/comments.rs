use crate::errors::{Error, ErrorType};
use crate::forms::comment::Comment;
use crate::models::comment::CommentId;
use crate::models::user::User;
use crate::{database, Resources, Template};
use axum::extract::{Extension, Form, Path};
use axum::headers::HeaderMap;
use axum::response::Redirect;
use axum::routing::{delete, get, post};
use axum::Router;
use serde_json::json;
use uuid::Uuid;

pub fn router() -> Router<Resources> {
    Router::new()
        .route("/new_comment/:scribble", get(get_new_comment_page))
        .route(
            "/new_comment/:scribble/:parent",
            get(get_new_comment_with_parent_page),
        )
        .route("/comment_vote/:entity_id/:positive", post(vote_on_comment))
        .route(
            "/comments/:entity_id",
            delete(delete_vote_on_comment).post(post_new_comment),
        )
}

#[tracing::instrument(skip(resources))]
pub async fn get_new_comment_page(
    Extension(resources): Extension<Resources>,
    Path(scribble_id): Path<Uuid>,
    user: User,
) -> Result<Template, Error> {
    tracing::info!("getting new comment");
    let scribble = database::scribbles::load_by_id(&resources.edge_db, scribble_id)
        .await?
        .ok_or_else(|| Error::not_found(scribble_id))?;

    Ok(Template::with_context(
        "new_comment",
        Some(user),
        json! {{"scribble": scribble}},
    ))
}

#[tracing::instrument(skip(resources))]
pub async fn get_new_comment_with_parent_page(
    Extension(resources): Extension<Resources>,
    Path((scribble_id, parent)): Path<(Uuid, Uuid)>,
    user: User,
) -> Result<Template, Error> {
    tracing::info!("getting new comment");
    let scribble = database::scribbles::load_by_id(&resources.edge_db, scribble_id)
        .await?
        .ok_or_else(|| Error::not_found(scribble_id))?;

    let parent = database::comments::get_by_id(&resources.edge_db, parent, Some(user.id)).await?;

    Ok(Template::with_context(
        "new_comment",
        Some(user),
        json! {{"parent": parent, "scribble": scribble}},
    ))
}

#[tracing::instrument(skip(resources))]
pub async fn post_new_comment(
    Extension(resources): Extension<Resources>,
    Path(scribble_id): Path<Uuid>,
    user: User,
    comment: Form<Comment>,
) -> Result<Redirect, Error> {
    let content = ammonia::clean(&comment.content);

    let CommentId { id } = database::comments::insert_comment(
        &resources.edge_db,
        scribble_id,
        user.id,
        &content,
        comment.parent,
    )
    .await?;

    database::comments::vote_on_comment(&resources.edge_db, id, true, user.id).await?;

    Ok(Redirect::to(&format!("/scribbles/{scribble_id}")))
}

#[tracing::instrument(skip(resources))]
pub async fn vote_on_comment(
    Extension(resources): Extension<Resources>,
    Path((comment_id, positive)): Path<(Uuid, bool)>,
    user: User,
    headers: HeaderMap,
) -> Result<Redirect, ErrorType> {
    // TODO fix transaction logic for edgedb
    // let mut transaction = resources.db.begin().await?;

    database::comments::vote_on_comment(&resources.edge_db, comment_id, positive, user.id).await?;

    // If rows_affected is 0 then this query just did an upsert that didn't change the value
    // if result.rows_affected() > 0 {
    //     let score = if positive { 1 } else { -1 };
    //
    //     database::users::add_to_user_score_for_comment(
    //         &mut transaction,
    //         user.id,
    //         comment_id,
    //         score,
    //     )
    //     .await?;
    // }

    // transaction.commit().await?;

    let referrer = headers
        .get("referer")
        .and_then(|h| h.to_str().ok())
        .unwrap_or("/");

    Ok(Redirect::to(referrer))
}

#[tracing::instrument(skip(resources))]
pub async fn delete_vote_on_comment(
    Extension(resources): Extension<Resources>,
    Path(comment_id): Path<Uuid>,
    user: User,
    headers: HeaderMap,
) -> Result<Redirect, ErrorType> {
    database::comments::delete_vote_on_comment(&resources.edge_db, comment_id, user.id).await?;

    let referrer = headers
        .get("referer")
        .and_then(|h| h.to_str().ok())
        .unwrap_or("/");

    Ok(Redirect::to(referrer))
}
