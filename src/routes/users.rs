use crate::auth::get_authorisation;
use crate::errors::{Error, ErrorType};
use crate::forms::rendering::RenderForm;
use crate::forms::users::NewUser;
use crate::models::user::{User, UserStats};
use crate::passwords::hash_password;
use crate::{database, emails, util, Resources, Responses, Template};
use axum::extract::{Extension, Form, Path, State};
use axum::response::Redirect;
use axum::routing::{get, post};
use axum::Router;
use axum_flash::{Flash, IncomingFlashes};
use reqwest::Response;
use serde_json::json;
use tower_cookies::Cookies;
use uuid::Uuid;

pub fn router() -> Router<Resources> {
    Router::new()
        .route("/new_user", get(new_user_form))
        .route("/users", post(new_user))
        .route("/users/:id", get(get_user))
        .route("/users/verify_email/:code", get(verify_email))
}

pub async fn new_user_form(current_user: Option<User>) -> Template {
    let context = json!({ "form": NewUser::render_empty() });

    Template::with_context("new_user", current_user, context)
}

#[axum_macros::debug_handler]
pub async fn new_user(
    State(resources): State<Resources>,
    cookies: Cookies,
    flash: Flash,
    form: Form<NewUser>,
) -> Result<Responses, Error> {
    if let Some(e) = form.get_errors_optional() {
        let context = json!({ "form": form.render_with_errors(e) });

        return Ok(Responses::Template(Template::with_context(
            "new_user", None, context,
        )));
    }

    if database::users::does_user_exist(&resources.edge_db, &form.username).await? {
        let mut errors = form.get_errors();
        errors.field.insert(
            String::from("username"),
            format!("Username, {}, is already taken", form.username),
        );

        let context = json!({
            "form": form.render_with_errors(errors)
        });

        Ok(Responses::Template(Template::with_context(
            "new_user", None, context,
        )))
    } else {
        let password_hash = hash_password(&form.password)?;
        let user = database::users::create_user(
            &resources.edge_db,
            &form.username,
            &password_hash,
            form.email.as_deref(),
        )
        .await?;

        tracing::info!(message="Created user", id=%user.id, username=%user.username);

        util::set_login_cookie(&cookies.private(&resources.cookie_key), user.id);

        if let Some(email) = form.email.as_deref() {
            if !email.is_empty() {
                send_welcome_email(&form, &resources, user.id, email).await?;
            }
        }

        let flash = flash.info("Successfully signed up");

        Ok(Responses::FlashRedirect((
            flash,
            Redirect::to(&format!("/users/{}", user.id)),
        )))
    }
}

#[tracing::instrument(skip(resources))]
async fn send_welcome_email(
    form: &Form<NewUser>,
    resources: &Resources,
    user_id: Uuid,
    email: &str,
) -> Result<Response, Error> {
    let code = database::users::load_user(&resources.edge_db, user_id)
        .await?
        .verification_code
        .ok_or_else(|| ErrorType::internal("No Verification code was generated for the user"))?;

    let params = emails::WelcomeParams {
        username: form.username.to_string(),
        verification_link: format!(
            "https://bardicscribbles.com/user/email_verification/{}",
            code
        ),
    };

    emails::send_welcome_email(email, params).await
}

#[derive(serde::Serialize)]
struct UserParams {
    current_user: Option<User>,
    user: User,
}

#[tracing::instrument(skip(resources))]
pub async fn get_user(
    Path(id): Path<Uuid>,
    resources: Extension<Resources>,
    current_user: Option<User>,
    flashes: IncomingFlashes,
) -> Result<Template, Error> {
    let user = database::users::load_user(&resources.edge_db, id).await?;

    let scribbles = database::scribbles::load_scribbles_for_user(&resources.edge_db, id).await?;
    let (user_collections, _stats) = futures::future::try_join(
        database::collections::load_collections_for_user(&resources.edge_db, id),
        database::users::get_user_stats(&resources.edge_db, id),
    )
    .await?;

    let auth = get_authorisation(current_user.as_ref(), &user);
    auth.view_or_error()?;

    let is_owner = current_user
        .as_ref()
        .map(|current| current.id == id)
        .unwrap_or(false);

    let context = serde_json::json!({
        "is_owner": is_owner,
        "user": user,
        "scribbles": scribbles,
        "user_collections": user_collections,
        // TODO fix stats
        "stats": UserStats {
            scribble_count: 0,
            comment_count: 0,
        },
        "flashes": util::get_flashes(&flashes)
    });

    Ok(Template::with_context("user", current_user, context))
}

pub async fn verify_email(
    resources: Extension<Resources>,
    Path(code): Path<Uuid>,
    user: Option<User>,
) -> Result<Template, Error> {
    if user.as_ref().map(|u| u.verified_email).unwrap_or(false) {
        return Err(Error::bad_request("Your account is already verified"));
    }

    database::users::verify_email(&resources.edge_db, code).await?;

    Ok(Template::no_context("verified_email", user))
}
