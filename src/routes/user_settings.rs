use crate::errors::Error;
use crate::forms::rendering::RenderForm;
use crate::forms::user_settings::UserSettings;
use crate::{database, Resources, Template, User};
use axum::extract::{Extension, Form};
use axum::routing::get;
use axum::Router;
use serde_json::json;

pub fn router() -> Router<Resources> {
    Router::new().route("/user_settings", get(get_settings_page).post(edit_settings))
}

async fn get_settings_page(user: User) -> Template {
    let form = UserSettings {
        username: user.username.clone(),
        email: user.email.clone(),
    };

    let context = json!({ "user": user, "form": form.render()});

    Template::with_context("user_settings", Some(user), context)
}

async fn edit_settings(
    resources: Extension<Resources>,
    user: User,
    Form(settings): Form<UserSettings>,
) -> Result<Template, Error> {
    if let Some(e) = settings.get_errors_optional() {
        let context = json!({"form": settings.render_with_errors(e)});

        return Ok(Template::with_context("user_settings", Some(user), context));
    }

    database::users::update_user_settings(
        &resources.edge_db,
        user.id,
        &settings.username,
        settings.email.as_deref(),
    )
    .await?;

    // Invalidate cached user. This will only invalidate the process cache, but it should
    // be enough to show users the updated data on the settings page
    if let Some(user_cache) = crate::extractors::USER_CACHE.get() {
        user_cache.invalidate(&user.id).await
    }

    // Reload user to get changes
    let user = database::users::load_user(&resources.edge_db, user.id).await?;

    let form = UserSettings {
        username: user.username.clone(),
        email: user.email.clone(),
    };

    let context = json!({ "user": user, "form": form.render()});

    Ok(Template::with_context("user_settings", Some(user), context))
}
