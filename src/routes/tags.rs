use axum::extract::{Extension, Form, Path};
use axum::response::Redirect;
use axum::routing::get;
use axum::{Json, Router};

use serde_json::json;

use crate::auth::get_authorisation;
use crate::auth::Resource::AdminResource;
use crate::errors::Error;
use crate::forms::tags::Tag;
use crate::models::user::User;
use crate::{database, Resources, Template};

pub fn router() -> Router<Resources> {
    Router::new()
        .route("/tags", get(get_all_tags).post(post_new_tag))
        .route("/tags_json", get(get_all_tags_json))
        .route("/tags/:tag/scribbles", get(get_scribbles_for_tag))
}

#[tracing::instrument(skip(resources))]
pub async fn get_all_tags(
    Extension(resources): Extension<Resources>,
    current_user: Option<User>,
) -> Result<Template, Error> {
    let tags = database::tags::get_all_tags(&resources.edge_db).await?;

    Ok(Template::with_context(
        "tags",
        current_user,
        json!({ "tags": tags }),
    ))
}

#[tracing::instrument(skip(resources))]
pub async fn get_all_tags_json(
    Extension(resources): Extension<Resources>,
    _current_user: User,
) -> Result<Json<Vec<String>>, Error> {
    let tags = database::tags::get_all_tags(&resources.edge_db).await?;

    Ok(Json(tags.into_iter().map(|tag| tag.name).collect()))
}

#[tracing::instrument(skip(resources))]
pub async fn post_new_tag(
    Extension(resources): Extension<Resources>,
    user: User,
    new_tag: Form<Tag>,
) -> Result<Redirect, Error> {
    get_authorisation(&user, AdminResource).edit_or_error()?;

    database::tags::insert_new_tag(
        &resources.edge_db,
        &new_tag.name,
        &new_tag.description,
        user.id,
    )
    .await?;

    Ok(Redirect::to("/tags"))
}

#[tracing::instrument(skip(resources))]
pub async fn get_scribbles_for_tag<'a>(
    Extension(resources): Extension<Resources>,
    Path(tag): Path<String>,
    user: Option<User>,
) -> Result<Template, Error> {
    let scribbles = database::tags::get_scribbles_for_tag(&resources.edge_db, &tag).await?;

    Ok(Template::with_context(
        "tag_page",
        user,
        json! {{"scribbles": scribbles, "tag_name": tag}},
    ))
}
