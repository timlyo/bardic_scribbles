use crate::Resources;
use axum::extract::OriginalUri;
use axum::http::StatusCode;
use axum::response::IntoResponse;
use axum::Router;
use axum_extra::routing::SpaRouter;

mod admin;
mod collections;
mod comments;
mod index;
mod info;
mod scribbles;
mod search;
mod session;
mod tags;
mod user_settings;
mod users;
mod votes;

pub fn router() -> Router<Resources> {
    Router::new()
        .merge(admin::router())
        .merge(collections::router())
        .merge(comments::router())
        .merge(index::router())
        .merge(scribbles::router())
        .merge(search::router())
        .merge(session::router())
        .merge(tags::router())
        .merge(user_settings::router())
        .merge(users::router())
        .merge(votes::router())
        .merge(info::router())
        .merge(SpaRouter::new("/assets", "dist"))
        .fallback(handle_404)
}

async fn handle_404(OriginalUri(uri): OriginalUri) -> impl IntoResponse {
    // TODO render pretty template
    (StatusCode::NOT_FOUND, format!("Nothing found at {uri}"))
}
