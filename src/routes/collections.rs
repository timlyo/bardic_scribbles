use crate::errors::{Error, ErrorType};
use crate::forms::collection::Collection;
use crate::models::user::User;
use crate::{database, forms, Resources, Template};
use axum::extract::{Extension, Form, Path};
use axum::http::HeaderMap;
use axum::response::Redirect;
use axum::routing::{get, post};
use axum::Router;
use serde_json::json;

use crate::forms::rendering::RenderForm;

use crate::auth::get_authorisation;
use crate::models::collection::CollectionId;
use uuid::Uuid;

pub fn router() -> Router<Resources> {
    Router::new()
        .route("/new_collection", get(new_collection))
        .route("/collections", post(post_collection))
        .route("/collections/:collection", get(get_collection_by_id))
        .route(
            "/collections/:collection/:scribble",
            post(put_scribble_in_collection).delete(delete_scribble_in_collection),
        )
}

pub async fn new_collection(current_user: User) -> Result<Template, ErrorType> {
    let form = forms::collection::Collection::render_empty();

    Ok(Template::with_context(
        "new_collection",
        Some(current_user),
        json!({ "form": form }),
    ))
}

pub async fn get_collection_by_id(
    Extension(resources): Extension<Resources>,
    Path(id): Path<Uuid>,
    current_user: Option<User>,
) -> Result<Template, Error> {
    let collection = database::collections::get_collection_by_id(&resources.edge_db, id).await?;

    get_authorisation(current_user.as_ref(), &collection).view_or_error()?;

    let scribbles =
        database::collections::get_scribbles_in_collection(&resources.edge_db, id).await?;

    let owner = database::users::load_user(&resources.edge_db, collection.owner_id).await?;

    Ok(Template::with_context(
        "collection",
        current_user,
        json!({"collection": collection, "scribbles": scribbles, "owner": owner}),
    ))
}

pub async fn post_collection(
    Extension(resources): Extension<Resources>,
    current_user: User,
    form: Form<Collection>,
) -> Result<Redirect, ErrorType> {
    let CollectionId { id } = database::collections::new_collection(
        &resources.edge_db,
        current_user.id,
        &form.name,
        form.public,
    )
    .await?;

    Ok(Redirect::to(&format!("/collections/{id}")))
}

pub async fn put_scribble_in_collection(
    Extension(resources): Extension<Resources>,
    current_user: User,
    Path((collection, scribble)): Path<(Uuid, Uuid)>,
    headers: HeaderMap,
) -> Result<Redirect, Error> {
    let collection =
        database::collections::get_collection_by_id(&resources.edge_db, collection).await?;

    get_authorisation(&current_user, &collection).edit_or_error()?;

    database::collections::add_scribble_to_collection(&resources.edge_db, scribble, collection.id)
        .await?;

    let referrer = headers
        .get("referer")
        .and_then(|h| h.to_str().ok())
        .unwrap_or("/");

    Ok(Redirect::to(referrer))
}

pub async fn delete_scribble_in_collection(
    Extension(resources): Extension<Resources>,
    current_user: User,
    Path(collection): Path<Uuid>,
    Path(scribble): Path<Uuid>,
    headers: HeaderMap,
) -> Result<Redirect, Error> {
    let collection =
        database::collections::get_collection_by_id(&resources.edge_db, collection).await?;

    get_authorisation(&current_user, &collection).delete_or_error()?;

    database::collections::remove_scribble_from_collection(
        &resources.edge_db,
        scribble,
        collection.id,
    )
    .await?;

    let referrer = headers
        .get("referer")
        .and_then(|h| h.to_str().ok())
        .unwrap_or("/");

    Ok(Redirect::to(referrer))
}
