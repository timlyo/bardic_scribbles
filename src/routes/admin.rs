use crate::auth::get_authorisation;
use crate::auth::Resource::AdminResource;
use crate::emails::send_email;
use crate::errors::Error;
use crate::models::user::User;
use crate::{database, Resources, Template};
use axum::extract::Extension;
use axum::routing::{get, post};
use axum::Router;
use serde_json::json;

pub fn router() -> Router<Resources> {
    Router::new()
        .route("/admin", get(get_admin))
        .route("/send_test_email", post(send_test_email))
}

async fn get_admin(
    Extension(resources): Extension<Resources>,
    user: User,
) -> Result<Template, Error> {
    get_authorisation(&user, AdminResource).view_or_error()?;

    let (user_count, latest_user) = futures::future::try_join(
        database::users::get_user_count(&resources.edge_db),
        database::users::get_latest_user(&resources.edge_db),
    )
    .await?;

    Ok(Template::with_context(
        "admin",
        Some(user),
        json! {{"user_count": user_count, "latest_user": latest_user}},
    ))
}

async fn send_test_email(user: User) -> Result<String, String> {
    if user.email.is_none() {
        return Err("Set user email before sending".to_string());
    }

    send_email(
        user.email.as_deref().unwrap(),
        "Bardic Scribbles test email",
        "test email",
        "test",
    )
    .await
    .map(|r| format!("{:?}", r))
    .map_err(|error| error.to_string())
}
