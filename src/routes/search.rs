use crate::errors::Error;
use crate::models::scribble::ScribbleTag;
use crate::models::user::User;
use crate::{database, Resources, Template};
use axum::extract::{Extension, Query};
use axum::routing::get;
use axum::Router;
use itertools::Itertools;
use serde::{Deserialize, Serialize};
use serde_json::json;
use std::collections::HashSet;

pub fn router() -> Router<Resources> {
    Router::new().route("/search", get(get_search))
}

#[derive(Serialize)]
pub struct TagWithSelection {
    pub tag: ScribbleTag,
    pub selected: bool,
    /// Value that will be appended to the URL if this tag is clicked
    pub url_tag_value: String,
}

pub fn generate_tag_list(
    query_tags: Option<HashSet<&str>>,
    tag_list: Vec<ScribbleTag>,
) -> Vec<TagWithSelection> {
    if let Some(query_tags) = &query_tags {
        tag_list
            .into_iter()
            .map(|tag| {
                let selected = query_tags.contains(&tag.name.as_str());

                let url_tag_value = if selected {
                    query_tags
                        .iter()
                        .filter(|query_tag| query_tag != &&tag.name.as_str())
                        .join(",")
                } else {
                    format!("{},{}", query_tags.iter().join(","), tag.name)
                };

                TagWithSelection {
                    selected,
                    tag,
                    url_tag_value,
                }
            })
            .collect()
    } else {
        tag_list
            .into_iter()
            .map(|tag| TagWithSelection {
                selected: false,
                url_tag_value: tag.name.clone(),
                tag,
            })
            .collect()
    }
}

#[derive(Deserialize, Debug)]
pub struct SearchParams {
    query: String,
    tags: Option<String>,
}

#[tracing::instrument(skip(resources))]
pub async fn get_search(
    Extension(resources): Extension<Resources>,
    Query(params): Query<SearchParams>,
    user: Option<User>,
) -> Result<Template, Error> {
    tracing::info!(message = "Searching", query=%params.query, tags=?params.tags);
    let query_tags = params
        .tags
        .as_deref()
        .map(|t| t.split(',').into_iter().collect::<HashSet<&str>>());

    let scribbles =
        database::scribbles::search_scribbles(&resources.edge_db, &params.query, &query_tags)
            .await?;
    // TODO sort by usage
    let tag_list = database::tags::get_all_tags(&resources.edge_db).await?;

    let tag_list = generate_tag_list(query_tags, tag_list);

    Ok(Template::with_context(
        "search_results",
        user,
        json! {{"results": scribbles, "tag_list": tag_list, "search_query": params.query}},
    ))
}
