use axum::extract::Extension;

use axum::routing::get;
use axum::Router;

use serde_json::json;

use crate::errors::Error;
use crate::models::user::User;
use crate::{database, Resources, Template};

pub fn router() -> Router<Resources> {
    Router::new().route("/", get(get_index))
}

#[tracing::instrument(skip(resources))]
pub async fn get_index(
    Extension(resources): Extension<Resources>,
    current_user: Option<User>,
) -> Result<Template, Error> {
    let scribbles = database::scribbles::load_recent_scribble_summaries(&resources.edge_db).await?;

    Ok(Template {
        name: "index",
        current_user,
        context: json!({ "scribbles": scribbles }),
    })
}
