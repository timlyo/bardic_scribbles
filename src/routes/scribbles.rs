use crate::auth::get_authorisation;
use crate::errors::{Error, ErrorType};
use crate::forms::rendering::RenderForm;
use crate::forms::scribbles::ScribbleData;
use crate::models::comment::CommentSoup;
use crate::models::scribble::ScribbleId;
use crate::models::user::User;
use crate::{database, templates, Resources, Responses, Template};
use axum::extract::{Extension, Form, Path};
use axum::response::Redirect;
use axum::routing::{get, post};
use axum::Router;
use serde_json::json;
use uuid::Uuid;

pub fn router() -> Router<Resources> {
    Router::new()
        .route("/new_scribble", get(get_new_scribble))
        .route("/edit_scribble/:id", get(get_edit_scribble))
        .route("/scribbles/:id/versions", get(get_previous_versions))
        .route(
            "/scribbles/:id",
            get(get_scribble)
                .delete(delete_scribble)
                .post(edit_scribble),
        )
        .route("/scribbles", post(create_scribble))
}

#[tracing::instrument(skip(resources))]
pub async fn get_scribble(
    Path(id): Path<Uuid>,
    current_user: Option<User>,
    Extension(resources): Extension<Resources>,
) -> Result<Template, Error> {
    let user_data = if let Some(user) = &current_user {
        Some(
            futures::future::try_join(
                database::collections::get_collections_for_scribble_for_user(
                    &resources.edge_db,
                    id,
                    user.id,
                ),
                database::votes::load_user_votes_for_scribble(&resources.edge_db, id, user.id),
            )
            .await?,
        )
    } else {
        None
    };

    let (scribble, votes, raw_comments) = futures::future::try_join3(
        database::scribbles::load_by_id(&resources.edge_db, id),
        database::votes::load_votes_for_scribble(&resources.edge_db, id),
        database::comments::load_comments_for_scribble(
            &resources.edge_db,
            id,
            current_user.as_ref().map(|u| u.id),
        ),
    )
    .await?;

    let scribble = scribble.ok_or_else(|| Error::not_found(id))?;

    let comments = CommentSoup::from_comments(raw_comments).construct_tree();

    let (user_collections, user_votes) = if let Some(user_data) = user_data {
        (Some(user_data.0), Some(user_data.1))
    } else {
        (None, None)
    };

    let auth = get_authorisation(current_user.as_ref(), &scribble);
    auth.view_or_error()?;

    let is_owner = current_user
        .as_ref()
        .map(|current_user| current_user.id == scribble.creator.id)
        .unwrap_or(false);

    let vote_buttons = templates::VoteButtons {
        authorisation: auth,
        votes: votes.clone(),
        existing_user_vote: user_votes.clone(),
        is_owner,
        scribble_id: scribble.id,
    };

    let context = json!({
        "scribble": scribble,
        "comments": comments,
        "user_collections": user_collections,
        "vote_buttons": askama::Template::render(&vote_buttons)?
    });

    Ok(Template::with_context("scribble", current_user, context))
}

#[tracing::instrument()]
pub async fn get_new_scribble(current_user: User) -> Template {
    let form = ScribbleData::render_empty();

    Template::with_context("new_scribble", Some(current_user), json!({ "form": form }))
}

#[tracing::instrument(skip(resources))]
pub async fn get_edit_scribble(
    current_user: User,
    Path(id): Path<Uuid>,
    Extension(resources): Extension<Resources>,
) -> Result<Template, Error> {
    let scribble = database::scribbles::load_by_id(&resources.edge_db, id).await?;

    if scribble.is_none() {
        return Err(Error::not_found(id));
    }

    let scribble = scribble.unwrap();

    get_authorisation(Some(&current_user), &scribble).edit_or_error()?;

    Ok(Template::with_context(
        "edit_scribble",
        Some(current_user),
        json!({ "scribble": scribble }),
    ))
}

#[tracing::instrument(skip(resources))]
pub async fn edit_scribble(
    current_user: User,
    Path(id): Path<Uuid>,
    Extension(resources): Extension<Resources>,
    Form(form): Form<ScribbleData>,
) -> Result<Responses, Error> {
    let scribble = match database::scribbles::load_by_id(&resources.edge_db, id).await? {
        Some(scribble) => scribble,
        None => return Err(ErrorType::not_found(id).into()),
    };

    if let Some(e) = form.get_errors_optional() {
        let context = json!({"form": form.render_with_errors(e)});

        return Ok(Responses::Template(Template::with_context(
            "new_scribble",
            Some(current_user),
            context,
        )));
    }

    get_authorisation(&current_user, &scribble).edit_or_error()?;

    let tags = form.get_tags();
    let cleaned_content = ammonia::clean(&form.body);

    database::scribbles::edit_scribble_body(
        &resources.edge_db,
        id,
        &form.title,
        &cleaned_content,
        form.edit_reason.as_deref(),
        tags,
    )
    .await?;

    Ok(Responses::Redirect(Redirect::to(&format!(
        "/scribbles/{id}"
    ))))
}

pub async fn create_scribble(
    current_user: User,
    Extension(resources): Extension<Resources>,
    form: Form<ScribbleData>,
) -> Result<Responses, Error> {
    if let Some(e) = form.get_errors_optional() {
        let context = json!({"form": form.render_with_errors(e)});

        return Ok(Responses::Template(Template::with_context(
            "new_scribble",
            Some(current_user),
            context,
        )));
    }

    let tags = form.get_tags();
    let cleaned_content = ammonia::clean(&form.body);
    let cleaned_title = ammonia::clean(&form.title);

    let ScribbleId { id } = database::scribbles::insert_scribble(
        &resources.edge_db,
        current_user.id,
        &cleaned_title,
        &cleaned_content,
        tags.clone(),
    )
    .await?;

    tracing::info!(message="Created scribble", title=%form.title, tags=?tags);
    tracing::debug!(%form.body);

    Ok(Responses::Redirect(Redirect::to(&format!(
        "/scribbles/{id}"
    ))))
}

#[tracing::instrument(skip(resources))]
pub async fn delete_scribble(
    Path(id): Path<Uuid>,
    Extension(resources): Extension<Resources>,
    user: User,
) -> Result<Redirect, Error> {
    let scribble = database::scribbles::load_by_id(&resources.edge_db, id)
        .await?
        .ok_or_else(|| Error::not_found(id))?;

    get_authorisation(&user, &scribble).delete_or_error()?;

    database::scribbles::delete_scribble(&resources.edge_db, id).await?;

    Ok(Redirect::to("/"))
}

pub async fn get_previous_versions<'a>(
    Path(id): Path<Uuid>,
    Extension(resources): Extension<Resources>,
    user: User,
) -> Result<Template, Error> {
    let scribble = database::scribbles::load_by_id(&resources.edge_db, id).await?;
    let versions = database::scribbles::load_previous_versions(&resources.edge_db, id).await?;

    if let Some(scribble) = scribble {
        Ok(Template::with_context(
            "scribble_versions",
            Some(user),
            json!({ "versions": versions, "scribble": scribble }),
        ))
    } else {
        Err(Error::not_found(id))
    }
}
