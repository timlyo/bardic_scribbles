use crate::errors::{Error, ErrorType};
use crate::models::user::User;
use crate::{database, forms, Resources, Template};
use axum::extract::{Extension, Form, Path};
use axum::http::HeaderMap;
use axum::response::Redirect;
use axum::routing::{get, post};
use axum::Router;
use serde_json::json;

use crate::auth::get_authorisation;
use crate::auth::Resource::ModeratorResource;
use uuid::Uuid;

pub fn router() -> Router<Resources> {
    Router::new()
        .route("/votes/:scribble_id", post(vote_on_scribble))
        .route("/votes/:scribble_id/list", get(show_scribble_votes))
}

#[tracing::instrument(skip(resources, headers))]
async fn vote_on_scribble(
    Extension(resources): Extension<Resources>,
    Path(scribble_id): Path<Uuid>,
    current_user: User,
    headers: HeaderMap,
    form: Form<forms::votes::Vote>,
) -> Result<Redirect, Error> {
    let scribble = database::scribbles::load_by_id(&resources.edge_db, scribble_id)
        .await?
        .ok_or_else(|| ErrorType::not_found(scribble_id))?;

    get_authorisation(&current_user, &scribble).vote_or_error(form.vote)?;

    let exists = database::votes::does_vote_exist(
        &resources.edge_db,
        scribble_id,
        form.vote,
        current_user.id,
    )
    .await?;

    if exists {
        tracing::info!(message = "Removing vote");
        database::votes::remove_vote_from_scribble(
            &resources.edge_db,
            scribble_id,
            form.vote,
            current_user.id,
        )
        .await?;
    } else {
        tracing::info!(message = "Adding vote");
        database::votes::add_vote_to_scribble(
            &resources.edge_db,
            scribble_id,
            form.vote,
            current_user.id,
            form.reason.as_deref(),
        )
        .await?;
    }

    let referrer = headers
        .get("referer")
        .and_then(|h| h.to_str().ok())
        .unwrap_or("/");

    Ok(Redirect::to(referrer))
}

async fn show_scribble_votes(
    Extension(resources): Extension<Resources>,
    Path(scribble_id): Path<Uuid>,
    current_user: User,
) -> Result<Template, Error> {
    get_authorisation(&current_user, ModeratorResource).view_or_error()?;

    let votes = database::votes::list_votes_for_scribble(&resources.edge_db, scribble_id).await?;

    let context = json!({ "votes": votes });

    Ok(Template::with_context(
        "vote_list",
        Some(current_user),
        context,
    ))
}
