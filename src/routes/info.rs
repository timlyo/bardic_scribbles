use crate::errors::Error;
use crate::models::user::User;
use crate::templates::Template;
use crate::Resources;
use axum::extract::Path;
use axum::routing::get;
use axum::Router;

pub fn router() -> Router<Resources> {
    Router::new().route("/info/:title", get(get_info_page))
}

async fn get_info_page(Path(title): Path<String>, user: Option<User>) -> Result<Template, Error> {
    match title.as_str() {
        "voting" => Ok(Template::no_context("info_voting", user)),
        other => Err(Error::not_found(other)),
    }
}
