use crate::database::users::EdgeDbError;
use crate::errors::Error;
use crate::models::scribble::{Scribble, ScribbleBody, ScribbleId, ScribbleSummary};
use edgedb_protocol::model::Json;
use edgedb_protocol::value::Value;
use std::collections::HashSet;
use uuid::Uuid;

#[tracing::instrument(skip(db))]
pub async fn load_recent_scribble_summaries(
    db: &edgedb_tokio::Client,
) -> Result<Vec<ScribbleSummary>, Error> {
    db.query(
        r#"
    select Scribble {
        id,
        title := .latest_body.title,
        created,
        creator: {
            id,
            username,
            user_type_str,
        },
        tags: {
            id,
            name 
        },
        points,
    } 
        order by .created desc
        limit 10
    "#,
        &(),
    )
    .await
    .map_err(Error::from)
}

#[tracing::instrument(skip(db))]
pub async fn load_by_id(db: &edgedb_tokio::Client, id: Uuid) -> Result<Option<Scribble>, Error> {
    db.query_single(
        r#"
        select Scribble {
            id,
            created,
            creator: {
                id,
                username,
                user_type_str,
            },
            latest_body: {
                id,
                created,
                title,
                body,
                edit_reason
            },
            tags: {
                id,
                name,
                description
            },
            points,
        } filter .id = <uuid> $0
    "#,
        &(id,),
    )
    .await
    .map_err(Error::from)
}

#[tracing::instrument(skip(db))]
pub async fn insert_scribble(
    db: &edgedb_tokio::Client,
    creator: Uuid,
    title: &str,
    body: &str,
    tags: HashSet<&str>,
) -> Result<ScribbleId, EdgeDbError> {
    let tags = unsafe { Json::new_unchecked(serde_json::to_string(&tags).unwrap()) };

    db.query_required_single(
        r#"
        with
            tags := (
                for tag in json_array_unpack(<json> $3) union (
                    insert ScribbleTag {
                        name := <str> tag
                    } unless conflict on .name else ScribbleTag 
                )
            )
        insert Scribble {
            creator := (select User filter .id = <uuid> $0),
            bodies := (
                insert ScribbleBody {
                    title := <str> $1,
                    body := <str> $2
                }
            ),
            tags := distinct tags
        }
        "#,
        &(creator, title, body, tags),
    )
    .await
}

/// Update scribble body, return Ok(true) if a new version was inserted
#[tracing::instrument(skip(db), err)]
pub async fn edit_scribble_body(
    db: &edgedb_tokio::Client,
    id: Uuid,
    title: &str,
    body: &str,
    reason: Option<&str>,
    tags: HashSet<&str>,
) -> Result<ScribbleId, Error> {
    let tags = unsafe { Json::new_unchecked(serde_json::to_string(&tags).unwrap()) };

    db.query_required_single(
        r#"
        update Scribble 
        filter .id = <uuid> $0
        set {
            bodies += (
                insert ScribbleBody {
                    title := <str> $1,
                    body := <str> $2,
                    edit_reason := <optional str> $3
                }
            ),
            tags := (
                for tag in json_array_unpack(<json> $4) union (
                    insert ScribbleTag {
                        name := <str> tag
                    } unless conflict on .name else ScribbleTag 
                )
            )
        } 
    "#,
        &(id, title, body, reason, tags),
    )
    .await
    .map_err(Error::from)
}

#[tracing::instrument(skip(db))]
pub async fn load_scribbles_for_user(
    db: &edgedb_tokio::Client,
    user_id: Uuid,
) -> Result<Vec<ScribbleSummary>, Error> {
    db.query(
        r#"
    select Scribble {
        id,
        title := .latest_body.title,
        created,
        creator: {
            id,
            username,
            user_type_str,
        },
        tags: {
            id,
            name,
        },
        points := 0,
    } filter .creator.id = <uuid> $0
    "#,
        &(user_id,),
    )
    .await
    .map_err(Error::from)
}

#[tracing::instrument(skip(db))]
pub async fn delete_scribble(db: &edgedb_tokio::Client, id: Uuid) -> Result<(), Error> {
    db.query_required_single(r#"delete Scribble filter .id = <uuid> $0"#, &(id,))
        .await
        .map(|_: Value| ())
        .map_err(Error::from)
}

#[tracing::instrument(skip(db))]
pub async fn search_scribbles(
    db: &edgedb_tokio::Client,
    query: &str,
    tags: &Option<HashSet<&str>>,
) -> Result<Vec<ScribbleSummary>, Error> {
    let _tags = unsafe { Json::new_unchecked(serde_json::to_string(&tags).unwrap()) };

    db.query(
        r#"
        select Scribble {
            id,
            title := .latest_body.title,
            created,
            creator: {
                id,
                username,
                user_type_str,
            },
            tags:{
                id,
                name,
            },
            points,
        } filter contains(.title, <str> $0) # TODO make this not suck
    "#,
        &(query,),
    )
    .await
    .map_err(Error::from)
}

#[tracing::instrument(skip(db))]
pub async fn load_previous_versions(
    db: &edgedb_tokio::Client,
    id: Uuid,
) -> Result<Vec<ScribbleBody>, Error> {
    db.query(
        r#"
    select ScribbleBody {
        id,
        created,
        title,
        body,
        edit_reason,
    } filter .<bodies[is Scribble].id = <uuid> $0
    "#,
        &(id,),
    )
    .await
    .map_err(Error::from)
}
