use crate::database::users::EdgeDbError;
use crate::errors::Error;
use crate::models::comment::{Comment, CommentId};
use edgedb_protocol::value::Value;
use uuid::Uuid;

#[tracing::instrument(skip(db))]
pub async fn insert_comment(
    db: &edgedb_tokio::Client,
    scribble: Uuid,
    creator: Uuid,
    content: &str,
    parent: Option<Uuid>,
) -> Result<CommentId, Error> {
    db.query_required_single(
        r#"
    insert Comment {
        scribble := (select Scribble filter .id = <uuid> $0),
        creator := (select User filter .id = <uuid> $1),
        parent := (select detached Comment filter .id = <optional uuid> $2),
        content := <str> $3
    }
    "#,
        &(scribble, creator, parent, content),
    )
    .await
    .map_err(Error::from)
}

#[tracing::instrument(skip(db))]
pub async fn get_by_id(
    db: &edgedb_tokio::Client,
    id: Uuid,
    current_user: Option<Uuid>,
) -> Result<Comment, Error> {
    db.query_required_single(r#"
        select Comment {
            id,
            creator: {
                id,
                username
            },
            content,
            created,
            positive_votes := count(CommentVote filter .comment.id = Comment.id and .positive = true),
            negative_votes := count(CommentVote filter .comment.id = Comment.id and .positive = false),
            user_positive_vote := (select CommentVote { positive } filter .comment.id = Comment.id and .voter.id = <optional uuid> $1 ).positive,
            parent,
            children,
        } filter .id = <uuid> $0
    "#, &(id, current_user)).await.map_err(Error::from)
}

#[tracing::instrument(skip(db))]
pub async fn load_comments_for_scribble(
    db: &edgedb_tokio::Client,
    scribble: Uuid,
    user: Option<Uuid>,
) -> Result<Vec<Comment>, Error> {
    db.query(
        // TODO make recursive once db supports it
        r#"
    select Comment {
        id,
        creator: {
            id,
            username
        },
        content,
        created,
        positive_votes := count(CommentVote filter .comment.id = Comment.id and .positive = true),
        negative_votes := count(CommentVote filter .comment.id = Comment.id and .positive = false),
        user_positive_vote := (select CommentVote { positive } filter .comment.id = Comment.id and .voter.id = <optional uuid> $1 ).positive,
        parent,
        children,
    } filter .scribble.id = <uuid> $0
    "#,
        &(scribble, user),
    )
    .await
    .map_err(Error::from)
}

#[tracing::instrument(skip(db))]
pub async fn vote_on_comment(
    db: &edgedb_tokio::Client,
    comment: Uuid,
    positive: bool,
    voter: Uuid,
) -> Result<(), EdgeDbError> {
    db.query_required_single(
        r#"
    insert CommentVote {
        comment := (select Comment filter .id = <uuid> $0),
        voter := (select User filter .id = <uuid> $1),
        positive := <bool> $2
    }
    unless conflict on (.comment, .voter)
    else (
        update CommentVote set { positive := <bool> $2 }
    )
    "#,
        &(comment, voter, positive),
    )
    .await
    .map(|_: Value| ())
}

pub async fn delete_vote_on_comment(
    db: &edgedb_tokio::Client,
    comment: Uuid,
    voter: Uuid,
) -> Result<(), EdgeDbError> {
    db.query_required_single(
        r#"
        delete CommentVote
        filter .comment.id = <uuid> $0 and .voter.id = <uuid> $1
    "#,
        &(comment, voter),
    )
    .await
    .map(|_: Value| ())
}
