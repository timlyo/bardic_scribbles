use crate::database::users::EdgeDbError;
use crate::errors::Error;
use crate::models::scribble::{ScribbleSummary, ScribbleTag};
use uuid::Uuid;

pub async fn get_all_tags(db: &edgedb_tokio::Client) -> Result<Vec<ScribbleTag>, Error> {
    db.query(
        r#"
    select ScribbleTag{
        id,
        name,
        description
    }"#,
        &(),
    )
    .await
    .map_err(Error::from)
}

pub async fn insert_new_tag(
    db: &edgedb_tokio::Client,
    name: &str,
    description: &str,
    user_id: Uuid,
) -> Result<(Uuid, Uuid), EdgeDbError> {
    db.query_required_single(
        r#"
    with
        new_tag := (insert ScribbleTag {
            name := <str> $0,
            description := <str> $1,
        }),
        log := (insert ModeratorNewTag {
            moderator := (select User filter .id = <uuid> $2),
            tag := new_tag
        })
    select (new_tag.id, log.id)
    "#,
        &(name, description, user_id),
    )
    .await
}

pub async fn get_scribbles_for_tag(
    db: &edgedb_tokio::Client,
    tag: &str,
) -> Result<Vec<ScribbleSummary>, Error> {
    db.query(
        r#"
    select Scribble {
        id,
        title := .latest_body.title,
        created,
        creator: {
            id,
            username,
            user_type_str,
        },
        tags: {
            id,
            name,
        },
        points := 0,
    } filter contains(.tags.name, <str> $0)
    "#,
        &(tag,),
    )
    .await
    .map_err(Error::from)
}
