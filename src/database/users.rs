use crate::errors::Error;
use crate::models::user::{User, UserDetailed, UserId, UserPasswordHash};
use edgedb_protocol::value::Value;
use uuid::Uuid;

pub type EdgeDbError = edgedb_tokio::Error;

pub async fn get_password_hash(
    db: &edgedb_tokio::Client,
    username: &str,
) -> Result<Option<UserPasswordHash>, edgedb_tokio::Error> {
    db.query_single(
        r"select User {id, password_hash}
    filter .username = <str>$0",
        &(username,),
    )
    .await
}

pub async fn create_user(
    db: &edgedb_tokio::Client,
    username: &str,
    password: &str,
    email: Option<&str>,
) -> Result<User, EdgeDbError> {
    db.query_required_single(
        r"select (insert User{
            username := <str>$0,
            password_hash := <str>$1,
            email := <optional str>$2
        }) {
            id, 
            username,
            created,
            email,
            user_type_str,
            points,
            verified_email,
            verification_code
        }",
        &(username, password, email),
    )
    .await
}

#[tracing::instrument(skip(db), err)]
pub async fn does_user_exist(
    db: &edgedb_tokio::Client,
    username: &str,
) -> Result<bool, EdgeDbError> {
    db.query_single::<Value, _>(
        r"select User {id}
        filter .username = <str> $0",
        &(username,),
    )
    .await
    .map(|user| user.is_some())
}

pub async fn load_user(db: &edgedb_tokio::Client, id: Uuid) -> Result<User, EdgeDbError> {
    db.query_required_single(
        r#"select User { 
            id, 
            username, 
            created, 
            email, 
            user_type_str, 
            points, 
            verified_email, 
            verification_code
        }
        filter User.id = <uuid> $0"#,
        &(id,),
    )
    .await
}

#[allow(dead_code)]
pub async fn load_user_detailed(
    db: &edgedb_tokio::Client,
    id: Uuid,
) -> Result<UserDetailed, EdgeDbError> {
    db.query_required_single(
        r#"select User { 
            id, 
            username, 
            created, 
            email, 
            user_type_str, 
            points, 
            verified_email, 
            verification_code,
            scribbles := (
                select Scribble {
                    id,
                    title := latest_body.title,
                    created,
                    creator,
                    tags,
                    points := 0
                }
            )
        }
        filter User.id = <uuid> $0"#,
        &(id,),
    )
    .await
}

pub async fn get_user_count(db: &edgedb_tokio::Client) -> Result<i64, EdgeDbError> {
    db.query_required_single(
        r#"
    select count(User)"#,
        &(),
    )
    .await
}

pub async fn get_latest_user(db: &edgedb_tokio::Client) -> Result<User, EdgeDbError> {
    db.query_required_single(
        r#"
        select User { id, username, created, email, user_type_str, points, verified_email, verification_code }
        order by .created
        limit 1
    "#,
        &(),
    )
    .await
}

#[tracing::instrument(skip(db), err)]
pub async fn get_user_stats(
    db: &edgedb_tokio::Client,
    user_id: Uuid,
) -> Result<Value, EdgeDbError> {
    db.query_required_single(
        r#"
    select {
        scribble_count := count(Scribble filter .creator.id = <uuid> $0),
        comment_count := count(Comment filter .creator.id = <uuid> $0)
    }
    "#,
        &(user_id,),
    )
    .await
}

pub async fn verify_email(db: &edgedb_tokio::Client, code: Uuid) -> Result<UserId, Error> {
    tracing::info!(message = "Verifying user code", %code);

    db.query_required_single(
        r"
    update User 
    filter .id = <uuid> $0
    set {
        verified_email := true,
        verification_code := {}
    }
    ",
        &(code,),
    )
    .await
    .map_err(Error::from)
}

pub async fn update_user_settings(
    db: &edgedb_tokio::Client,
    user_id: Uuid,
    username: &str,
    email: Option<&str>,
) -> Result<UserId, Error> {
    db.query_required_single(
        r#"
        update User
        filter .id = <uuid> $0
        set {
            username := <str> $1,
            email := <optional str> $2
        }
    "#,
        &(user_id, username, email),
    )
    .await
    .map_err(Error::from)
}
