use crate::errors::Error;
use crate::models::votes::{UserVotes, Vote, VoteCount, VoteCounts, VoteId, VoteType};
use std::collections::HashMap;
use uuid::Uuid;

pub async fn does_vote_exist(
    db: &edgedb_tokio::Client,
    scribble: Uuid,
    vote: VoteType,
    voter: Uuid,
) -> Result<bool, Error> {
    let vote: &'static str = vote.into();

    db.query_required_single(
        r#"
        select exists(
            select ScribbleVote
            filter .scribble.id = <uuid> $0
                and .vote_type = <VoteType> <str> $1
                and .voter.id = <uuid> $2
        )
    "#,
        &(scribble, vote, voter),
    )
    .await
    .map_err(Error::from)
}

#[tracing::instrument(skip(db))]
pub async fn add_vote_to_scribble(
    db: &edgedb_tokio::Client,
    scribble: Uuid,
    vote: VoteType,
    voter: Uuid,
    reason: Option<&str>,
) -> Result<VoteId, Error> {
    let (value, cost) = vote.value_and_cost();
    let vote: &'static str = vote.into();

    db.query_required_single(
        r#"
        insert ScribbleVote {
            scribble := (select Scribble filter .id = <uuid> $0),
            vote_type := <VoteType> <str> $1,
            voter := (select User filter .id = <uuid> $2),
            reason := <optional str> $3,
            value := <int32> $4,
            cost := <int32> $5,
        }
    "#,
        &(scribble, vote, voter, reason, value, cost),
    )
    .await
    .map_err(Error::from)
}

pub async fn remove_vote_from_scribble(
    db: &edgedb_tokio::Client,
    scribble: Uuid,
    vote: VoteType,
    voter: Uuid,
) -> Result<VoteId, Error> {
    let vote: &'static str = vote.into();

    db.query_required_single(
        r#"
        delete ScribbleVote
        filter .scribble.id = <uuid> $0
            and .vote_type = <VoteType> <str> $1
            and .voter.id = <uuid> $2
    "#,
        &(scribble, vote, voter),
    )
    .await
    .map_err(Error::from)
}

#[tracing::instrument(skip(db))]
pub async fn load_votes_for_scribble(
    db: &edgedb_tokio::Client,
    scribble: Uuid,
) -> Result<VoteCounts, Error> {
    let counts = db
        .query::<VoteCount, _>(
            r#"
        with
            votes := (select ScribbleVote filter .scribble.id = <uuid> $0),
            groups := (group votes by .vote_type )
        select groups {
            id,
            votes := (count(.elements)),
            vote_type := <str> .key.vote_type,
        }
    "#,
            &(scribble,),
        )
        .await?;

    let mut counts = counts
        .into_iter()
        .map(|count| (count.vote_type, count.votes))
        .collect::<HashMap<_, _>>();

    Ok(VoteCounts {
        basic: counts.remove("Basic").unwrap_or(0),
        detail: counts.remove("Detail").unwrap_or(0),
        unique: counts.remove("Unique").unwrap_or(0),
        modular: counts.remove("Modular").unwrap_or(0),
        super_vote: counts.remove("SuperVote").unwrap_or(0),
        unfocused: counts.remove("Unfocused").unwrap_or(0),
        needs_more_detail: counts.remove("NeedsMoreDetail").unwrap_or(0),
        problematic: counts.remove("Problematic").unwrap_or(0),
    })
}

#[tracing::instrument(skip(db))]
pub async fn load_user_votes_for_scribble(
    db: &edgedb_tokio::Client,
    scribble: Uuid,
    user: Uuid,
) -> Result<UserVotes, Error> {
    let votes = db
        .query::<Vote, _>(
            r#"
        select ScribbleVote {
            id,
            vote_type_str := <str> .vote_type,
            voter_name := .voter.username,
            voter_id := .voter.id,
            reason
        }
        filter .scribble.id = <uuid> $0 
            and .voter.id = <uuid> $1
    "#,
            &(scribble, user),
        )
        .await?;

    let mut votes = votes
        .into_iter()
        .map(|vote| (vote.vote_type_str.clone(), vote))
        .collect::<HashMap<_, _>>();

    Ok(UserVotes {
        basic: votes.remove("Basic"),
        detail: votes.remove("Detail"),
        unique: votes.remove("Unique"),
        modular: votes.remove("Modular"),
        super_vote: votes.remove("SuperVote"),
        unfocused: votes.remove("Unfocused"),
        needs_more_detail: votes.remove("NeedsMoreDetail"),
        problematic: votes.remove("Problematic"),
    })
}

pub async fn list_votes_for_scribble(
    db: &edgedb_tokio::Client,
    scribble: Uuid,
) -> Result<Vec<Vote>, Error> {
    db.query(
        r#"
        select ScribbleVote {
            id,
            vote_type,
            voter_name := .voter.name,
            voter_id := .voter.id,
            reason
        }
        filter .scribble.id = <uuid> $0
    "#,
        &(scribble,),
    )
    .await
    .map_err(Error::from)
}
