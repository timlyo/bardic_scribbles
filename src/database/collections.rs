use crate::database::users::EdgeDbError;
use crate::errors::Error;
use crate::models::collection::{Collection, CollectionForScribbleAndUser, CollectionId};
use crate::models::scribble::ScribbleSummary;
use uuid::Uuid;

pub async fn load_collections_for_user(
    db: &edgedb_tokio::Client,
    user_id: Uuid,
) -> Result<Vec<Collection>, EdgeDbError> {
    db.query(
        r#"
    select Collection {
        id,
        name,
        created,
        owner_id := .owner.id,
        public,
    } filter .owner.id = <uuid> $0
    "#,
        &(user_id,),
    )
    .await
}

pub async fn get_collection_by_id(
    db: &edgedb_tokio::Client,
    id: Uuid,
) -> Result<Collection, EdgeDbError> {
    db.query_required_single(
        r#"
        select Collection {
            id,
            name,
            created,
            owner_id := .owner.id,
            public,
        } filter .id = <uuid> $0
    "#,
        &(id,),
    )
    .await
}

pub async fn get_scribbles_in_collection(
    db: &edgedb_tokio::Client,
    id: Uuid,
) -> Result<Vec<ScribbleSummary>, EdgeDbError> {
    db.query(
        r#"
    select Scribble {
        id,
        title := .latest_body.title,
        created,
        creator: {
            id,
            username,
            user_type_str
        },
        tags: {
            id,
            name
        },
        points := 0,
    } filter .<scribbles[is Collection].id = <uuid> $0 and .active = true
    "#,
        &(id,),
    )
    .await
}

pub async fn new_collection(
    db: &edgedb_tokio::Client,
    user: Uuid,
    name: &str,
    public: bool,
) -> Result<CollectionId, EdgeDbError> {
    db.query_required_single(
        r#"
        insert Collection{
            name := <str> $1,
            owner := (select User filter .id = <uuid> $0),
            public := <bool> $2
        }
    "#,
        &(user, name, public),
    )
    .await
}

pub async fn add_scribble_to_collection(
    db: &edgedb_tokio::Client,
    scribble: Uuid,
    collection: Uuid,
) -> Result<CollectionId, EdgeDbError> {
    db.query_required_single(
        r#"
        update Collection
        filter .id = <uuid> $1
        set { 
            scribbles += (select Scribble filter .id = <uuid> $0)
        }
    "#,
        &(scribble, collection),
    )
    .await
}

pub async fn remove_scribble_from_collection(
    db: &edgedb_tokio::Client,
    scribble: Uuid,
    collection: Uuid,
) -> Result<CollectionId, EdgeDbError> {
    db.query_required_single(
        r#"
        update Collection
        filter .id = <uuid> $0
        set {
            .scribbles -= .scribbles.id = <uuid> $1
        }
    "#,
        &(collection, scribble),
    )
    .await
}

/// Load a list of a user's collections and set whether this scribble is within them or not
#[tracing::instrument(skip(db))]
pub async fn get_collections_for_scribble_for_user(
    db: &edgedb_tokio::Client,
    scribble: Uuid,
    user: Uuid,
) -> Result<Vec<CollectionForScribbleAndUser>, Error> {
    db.query(
        r#"
        select Collection {
            id,
            name,
            created,
            owner_id := .owner.id,
            public,
            scribble_in_collection := contains(array_agg(.scribbles.id), <uuid> $1)
        } filter .owner.id = <uuid> $0
    "#,
        &(user, scribble),
    )
    .await
    .map_err(Error::from)
}
