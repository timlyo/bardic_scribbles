CREATE MIGRATION m1p4eldk3h2ph3sjihbskaxqapk23s4j5vswndqzrczebxxg2s6sea
    ONTO m1cz2yn4qu5anwat244l4fymjfhab7a3oezo5sfnhhsicpdlz7mg4a
{
  ALTER TYPE default::ScribbleTag {
      ALTER PROPERTY description {
          RESET OPTIONALITY;
      };
  };
};
