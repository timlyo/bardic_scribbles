CREATE MIGRATION m1iwdv2xfutxb7ohhscqabnmfog5obekahvhsyszrvgl7gxzlffzza
    ONTO initial
{
  CREATE TYPE default::Collection {
      CREATE REQUIRED PROPERTY created -> std::datetime {
          SET default := (std::datetime_current());
          SET readonly := true;
      };
      CREATE REQUIRED PROPERTY name -> std::str;
      CREATE REQUIRED PROPERTY public -> std::bool {
          SET default := false;
      };
  };
  CREATE SCALAR TYPE default::UserType EXTENDING enum<Pleb, Moderator, Admin>;
  CREATE TYPE default::User {
      CREATE REQUIRED PROPERTY created -> std::datetime {
          SET default := (std::datetime_current());
          SET readonly := true;
      };
      CREATE PROPERTY email -> std::str;
      CREATE REQUIRED PROPERTY password_hash -> std::str;
      CREATE REQUIRED PROPERTY points -> std::int64 {
          SET default := 0;
      };
      CREATE REQUIRED PROPERTY user_type -> default::UserType {
          SET default := 'Pleb';
      };
      CREATE REQUIRED PROPERTY user_type_str := (<std::str>.user_type);
      CREATE REQUIRED PROPERTY username -> std::str {
          CREATE CONSTRAINT std::exclusive;
          CREATE CONSTRAINT std::min_len_value(3);
      };
      CREATE PROPERTY verification_code -> std::uuid {
          SET default := (std::uuid_generate_v4());
      };
      CREATE REQUIRED PROPERTY verified_email -> std::bool {
          SET default := false;
      };
  };
  ALTER TYPE default::Collection {
      CREATE REQUIRED LINK owner -> default::User;
  };
  CREATE TYPE default::ScribbleBody {
      CREATE REQUIRED PROPERTY body -> std::str;
      CREATE REQUIRED PROPERTY created -> std::datetime {
          SET default := (std::datetime_current());
          SET readonly := true;
      };
      CREATE PROPERTY edit_reason -> std::str;
      CREATE REQUIRED PROPERTY title -> std::str;
  };
  CREATE TYPE default::ScribbleTag {
      CREATE REQUIRED PROPERTY created -> std::datetime {
          SET default := (std::datetime_current());
          SET readonly := true;
      };
      CREATE REQUIRED PROPERTY description -> std::str;
      CREATE REQUIRED PROPERTY name -> std::str {
          CREATE CONSTRAINT std::exclusive;
      };
  };
  CREATE TYPE default::Scribble {
      CREATE REQUIRED LINK creator -> default::User;
      CREATE REQUIRED LINK latest_body -> default::ScribbleBody;
      CREATE MULTI LINK tags -> default::ScribbleTag;
      CREATE REQUIRED PROPERTY active -> std::bool {
          SET default := true;
      };
      CREATE REQUIRED PROPERTY created -> std::datetime {
          SET default := (std::datetime_current());
          SET readonly := true;
      };
  };
  ALTER TYPE default::Collection {
      CREATE MULTI LINK scribbles -> default::Scribble;
  };
  CREATE TYPE default::Comment {
      CREATE REQUIRED LINK creator -> default::User;
      CREATE LINK parent -> default::Comment;
      CREATE REQUIRED LINK scribble -> default::Scribble;
      CREATE REQUIRED PROPERTY content -> std::str;
      CREATE REQUIRED PROPERTY created -> std::datetime {
          SET default := (std::datetime_current());
          SET readonly := true;
      };
  };
  CREATE TYPE default::CommentVote {
      CREATE REQUIRED LINK comment -> default::Comment;
      CREATE REQUIRED LINK voter -> default::User;
      CREATE CONSTRAINT std::exclusive ON ((.voter, .comment));
      CREATE REQUIRED PROPERTY created -> std::datetime {
          SET default := (std::datetime_current());
          SET readonly := true;
      };
      CREATE REQUIRED PROPERTY positive -> std::bool;
  };
  CREATE ABSTRACT TYPE default::ModeratorAction {
      CREATE REQUIRED LINK moderator -> default::User;
      CREATE REQUIRED PROPERTY time -> std::datetime {
          SET default := (std::datetime_current());
          SET readonly := true;
      };
  };
  CREATE TYPE default::ModeratorNewTag EXTENDING default::ModeratorAction {
      CREATE REQUIRED LINK tag -> default::ScribbleTag;
  };
  CREATE SCALAR TYPE default::VoteType EXTENDING enum<Basic, Detail, Unique, Modular, SuperVote, Unfocused, NeedsMoreDetail, Problematic>;
  CREATE TYPE default::ScribbleVote {
      CREATE REQUIRED LINK scribble -> default::Scribble;
      CREATE REQUIRED PROPERTY value -> std::int32;
      CREATE REQUIRED LINK voter -> default::User;
      CREATE REQUIRED PROPERTY vote_type -> default::VoteType;
      CREATE CONSTRAINT std::exclusive ON ((.scribble, .vote_type, .voter));
      CREATE REQUIRED PROPERTY cost -> std::int32;
      CREATE REQUIRED PROPERTY created -> std::datetime {
          SET default := (std::datetime_current());
          SET readonly := true;
      };
      CREATE PROPERTY reason -> std::str;
  };
  ALTER TYPE default::Scribble {
      CREATE MULTI LINK votes := (SELECT
          .<scribble[IS default::ScribbleVote]
      FILTER
          (.scribble.id = .id)
      );
      CREATE REQUIRED PROPERTY points := (std::sum(.votes.value));
  };
  ALTER TYPE default::User {
      CREATE MULTI LINK votes_cast := (.<voter[IS default::ScribbleVote]);
  };
};
