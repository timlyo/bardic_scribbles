CREATE MIGRATION m1cz2yn4qu5anwat244l4fymjfhab7a3oezo5sfnhhsicpdlz7mg4a
    ONTO m1q5tvsxk6fly4nqztmcbsa2no75fgljjl5a42xcz2rqfrksvqf5qa
{
  ALTER TYPE default::Scribble {
      ALTER LINK latest_body {
          USING (std::assert_exists((SELECT
              .bodies ORDER BY
                  .created ASC
          LIMIT
              1
          )));
      };
  };
};
