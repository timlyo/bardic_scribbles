CREATE MIGRATION m1q5tvsxk6fly4nqztmcbsa2no75fgljjl5a42xcz2rqfrksvqf5qa
    ONTO m1iwdv2xfutxb7ohhscqabnmfog5obekahvhsyszrvgl7gxzlffzza
{
  ALTER TYPE default::Comment {
      CREATE MULTI LINK children := (.<parent[IS default::Comment]);
  };
  ALTER TYPE default::Scribble {
      CREATE REQUIRED MULTI LINK bodies -> default::ScribbleBody {
          SET REQUIRED USING (SELECT
              default::ScribbleBody 
          LIMIT
              1
          );
      };
  };
  ALTER TYPE default::Scribble {
      ALTER LINK latest_body {
          USING (std::assert_exists((SELECT
              default::ScribbleBody ORDER BY
                  .created ASC
          LIMIT
              1
          )));
      };
  };
  ALTER TYPE default::Scribble {
      ALTER LINK votes {
          USING (SELECT
              .<scribble[IS default::ScribbleVote]
          );
      };
  };
};
