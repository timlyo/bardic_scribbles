module default {
    scalar type UserType extending enum<Pleb, Moderator, Admin>;
    scalar type VoteType extending enum<Basic, Detail, Unique, Modular, SuperVote, Unfocused, NeedsMoreDetail, Problematic>;

    type User {
        required property username -> str{
            constraint exclusive;
            constraint min_len_value(3);
        }
        required property created -> datetime{
            default := datetime_current();
            readonly := true;
        }
        required property password_hash -> str;
        property email -> str;
        required property user_type -> UserType{
            default := "Pleb";
        }
        required property user_type_str := (<str> .user_type);
        required property points -> int64 {
            default := 0;
        }
        required property verified_email -> bool {
            default := false;
        }
        property verification_code -> uuid{
            default := uuid_generate_v4();
        }

        multi link votes_cast := .<voter[is ScribbleVote];
    }

    type Scribble {
        required link creator -> User;
        required property created -> datetime {
            default := datetime_current();
            readonly := true;
        }
        required property active -> bool {
            default := true
        }

        multi link votes := (select .<scribble[is ScribbleVote]);
        required property points := sum(.votes.value);

        required link latest_body := (
            assert_exists((select .bodies order by .created limit 1))
        );
        required multi link bodies -> ScribbleBody;
        multi link tags -> ScribbleTag;
    }

    type ScribbleBody {
        required property created -> datetime {
            default := datetime_current();
            readonly := true;
        }
        required property title -> str;
        required property body -> str;
        property edit_reason -> str;
    }

    type ScribbleTag {
        required property created -> datetime {
            default := datetime_current();
            readonly := true;
        }

        required property name -> str{
            constraint exclusive;
        }
        property description -> str;
    }

    type ScribbleVote {
        required property created -> datetime {
            default := datetime_current();
            readonly := true;
        }
        required link scribble -> Scribble;
        required property vote_type -> VoteType;
        required link voter -> User;

        required property value -> int32;
        required property cost -> int32;

        property reason -> str;

        constraint exclusive on ((.scribble, .vote_type, .voter));
    }

    type Comment {
        required property created -> datetime {
            default := datetime_current();
            readonly := true;
        }
        required property content -> str;

        required link scribble -> Scribble;
        required link creator -> User;

        link parent -> Comment;
        multi link children := .<parent[is Comment];
    }

    type CommentVote {
        required property created -> datetime {
            default := datetime_current();
            readonly := true;
        }
        required link voter -> User;
        required link comment -> Comment;
        required property positive -> bool;

        constraint exclusive on ((.voter, .comment));
    }

    type Collection {
        required property name -> str;
        required property created -> datetime {
            default := datetime_current();
            readonly := true;
        }
        required property public -> bool {
            default := false;
        }

        required link owner -> User;
        multi link scribbles -> Scribble;
    }

    abstract type ModeratorAction {
        required link moderator -> User;
        required property time -> datetime {
            default := datetime_current();
            readonly := true;
        }
    }

    type ModeratorNewTag extending ModeratorAction {
        required link tag -> ScribbleTag;
    }
}
